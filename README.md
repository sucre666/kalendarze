# CHANGELOG #

### 1.1.0 ###

w trakcie

### 1.0.0 ###
* dodanie kadrowania
* dodanie komponentu do skalowania obrazków
* dodanie lintera dla kodu JS (ESLint) i SASS
* dodaie SASS'a i refaktoryzacja kodu CSS
* dodanie Gulp'a oraz minifikacji i obfuskacji kodu
* dodanie autoprefixer'a dla lepszego wsparcia crossbrowser
* podniesienie wersji FabricJS do v.1.6.3
* usprawnienie i ujednolicenie zapisu obrazków (zdjęć/teł itp) na serwerze
* usunięcie starych/zalegających plików na serwerze
* poprawienie pojawiania się czarnego tła przy dodawaniu obrazka jako tło
* poprawienie braku wyświetlania się dni w kalendarium
* poprawienie ostylowania w zakładce Pomoc