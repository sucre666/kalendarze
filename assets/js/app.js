'use strict';
/* global angular */
angular.module('common.fabric', ['common.fabric.window', 'common.fabric.directive', 'common.fabric.canvas',
  'common.fabric.dirtyStatus']).factory('Fabric', ['FabricWindow', '$timeout', '$window', 'FabricCanvas',
    'FabricDirtyStatus', '$rootScope', '$location', function(FabricWindow, $timeout, $window, FabricCanvas,
                                                              FabricDirtyStatus, $rootScope, $location) {
      return function(options) {
        var canvas;
        var JSONObject;
        var clipboard = false;
        var cutting = false;
        var self = angular.extend({
          canvasBackgroundColor: '#ffffff',
          canvasBackgroundImage: '',
          canvasRedEyes: [],
          grid: 0,
          gridOriginal: 0,
          dragToGrid: false,
          canvasWidth: 300,
          canvasHeight: 300,
          canvasOriginalHeight: 300,
          canvasOriginalWidth: 300,
          maxContinuousRenderLoops: 25,
          continuousRenderTimeDelay: 100,
          editable: true,
          JSONExportProperties: [],
          loading: false,
          dirty: false,
          initialized: false,
          userHasClickedCanvas: false,
          downloadMultiplier: 3,
          runningIndex: 1E5,
          previousSelected: null,
          dragHolder: null,
          currentChange: 0,
          dragInit: false,
          renderCount: 0,
          imageDefaults: {},
          textDefaults: {},
          shapeDefaults: {
            originX: 'center',
            originY: 'center'
          },
          windowDefaults: {
            cornerSize: 8,
            borderColor: 'black',
            cornerColor: 'black',
            transparentCorners: false,
            rotatingPointOffset: 25
          },
          canvasDefaults: {
            selection: true,
            renderOnAddRemove: false
          }
        }, options);

        /**
         * capitalize words
         * @param {string} str
         * @return {string|*}
         */
        function capitalizeWords(str) {
          var i, _word, firLet, rest, words = str.split(' ');
          for (i = 0; i < words.length; i += 1) {
            _word = words[i];
            firLet = _word.substr(0, 1);
            rest = _word.substr(1, _word.length - 1);
            words[i] = firLet.toUpperCase() + rest;
          }
          return words.join(' ');
        }

        /**
         * get active style
         * @param {string} styleName
         * @param {object} object
         * @return {*}
         */
        function getActiveStyle(styleName, object) {
          object = object || canvas.getActiveObject();
          if (typeof object !== 'object' || object === null) {
            return '';
          }

          if (object.getSelectionStyles && object.isEditing) {
            return object.getSelectionStyles()[styleName] || '';
          }

          return object[styleName] || '';
        }

        /**
         * set active style
         * @param {string} styleName
         * @param {string} value
         * @param {object} object
         */
        function setActiveStyle(styleName, value, object) {
          object = object || canvas.getActiveObject();

          if (typeof object !== 'object' || object === null) {
            return;
          }

          var style = {};
          if (object.setSelectionStyles && object.isEditing) {
            style[styleName] = value;
            object.setSelectionStyles(style);
          } else {
            object[styleName] = value;
          }
        }

        /**
         * get active prop
         * @param {string} name
         * @return {string}
         */
        function getActiveProp(name) {
          var object = canvas.getActiveObject();
          return typeof object === 'object' && object !== null ? object[name] : '';
        }

        /**
         * set active prop
         * @param {string} name
         * @param {string} value
         */
        function setActiveProp(name, value) {
          var object = canvas.getActiveObject();
          if (typeof object === 'object' && object !== null) {
            object.set(name, value);
          }
        }

        /**
         * b64 to blob
         * @param {object} b64Data
         * @param {object} contentType
         * @param {number} sliceSize
         * @return {*}
         */
        function b64toBlob(b64Data, contentType, sliceSize) {
          contentType = contentType || '';
          sliceSize = sliceSize || 512;
          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);

            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
          return new Blob(byteArrays, {type: contentType});
        }

        /**
         * is hex
         * @param {string} str
         * @return {boolean}
         */
        function isHex(str) {
          return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/gi.test(str);
        }

        fabric.Canvas.prototype.removeItems = function(name) {
          var objectList = [], objects = this.getObjects();
          for (var i = objects.length - 1; i > -1; i--) {
            if (objects[i].mt && objects[i].mt === name) {
              canvas.remove(objects[i]);
            }
          }
          return objectList;
        };

        fabric.Object.prototype.toObject = (function(toObject) {
          return function() {
            return fabric.util.object.extend(toObject.call(this), {
              selectable: this.selectable,
              src: this.src,
              orgSrc: this.orgSrc,
              mt: this.mt,
              cs: this.cs,
              cx: this.cx,
              cy: this.cy,
              cw: this.cw,
              ch: this.ch,
              originalW: this.originalW,
              originalH: this.originalH,
              skewX: fabric.util.toFixed(this.skewX, fabric.Object.NUM_FRACTION_DIGITS),
              skewY: fabric.util.toFixed(this.skewY, fabric.Object.NUM_FRACTION_DIGITS)
            });
          };
        })(fabric.Object.prototype.toObject);

        fabric.Object.prototype.initialize = function(options) {
          if (!options) {
            return;
          }

          this.setOptions(options);
          if (options.mt === 2) {
            this.setControlsVisibility({
              ml: false,
              mr: false,
              mt: false,
              mb: false
            });
          }
        };

        fabric.Image.prototype.initialize = function(element, options) {
          options = options || {};
          this.filters = [];
          this.resizeFilters = [];
          this.callSuper('initialize', options);
          this.set({
            orgSrc: element.src,
            cx: 0,
            cy: 0,
            cw: element.width,
            ch: element.height,
            originalW: element.width,
            originalH: element.height
          });
          this._initElement(element, options);
        };

        fabric.Image.prototype.rerender = function(callback) {
          var img = new Image(),
            obj = this;

          img.onload = function() {
            var canvas = fabric.util.createCanvasElement();
            canvas.width = obj.width * obj.scaleX;
            canvas.height = obj.height * obj.scaleY;

            canvas.getContext('2d').drawImage(this, obj.cx, obj.cy, obj.cw, obj.ch, 0, 0,
                        canvas.width, canvas.height);
            img.onload = function() {
              obj.setElement(this);
              obj.applyFilters(callback);
              obj.set({
                left: obj.left,
                top: obj.top,
                angle: obj.angle,
                scaleX: 1,
                scaleY: 1
              });
              obj.setCoords();
              if (callback) {
                callback(obj);
              }
            };
            img.src = canvas.toDataURL('image/png');
          };
          img.src = this.orgSrc;
        };

        fabric.Image.prototype.zoomBy = function(callback) {
          if (this.cw > this.originalW) {
                    // this.cw = this.originalW;
          }
          if (this.ch > this.originalH) {
                    // this.ch = this.originalH;
          }
          if (this.cw < 1) {
            this.cw = 1;
          }
          if (this.ch < 1) {
            this.ch = 1;
          }
          if (this.cx < 0) {
            this.cx = 0;
          }
          if (this.cy < 0) {
            this.cy = 0;
          }
          if (this.cx > this.originalW - this.cw) {
            this.cx = this.originalW - this.cw;
          }
          if (this.cy > this.originalH - this.ch) {
            this.cy = this.originalH - this.ch;
          }

          this.rerender(callback);
        };

        self.render = function() {
          var objects = canvas.getObjects();

          for (var i in objects) {
            if (objects.hasOwnProperty(i)) {
              objects[i].setCoords();
            }
          }

          canvas.calcOffset();
          canvas.renderAll();
          self.renderCount++;
          console.log('Render cycle:', self.renderCount);
        };
        self.setupFilter = function(data) {
          if (data.label.length <= 0) {
            return;
          }

          var filterName = data.label,
            filterSrc = data.src,
            object = canvas.getActiveObject();

          if (!object || object.type !== 'image') {
            return;
          }

          switch (true) {
          case /filters/.test(filterSrc):
            object.filters = object.filters.slice(0, 1);
            switch (filterName) {
              case 'Brak':
                break;
              case 'Sharpen':
                object.filters.push(new fabric.Image.filters.Convolute({
                  matrix: [0, -1, 0, -1, 5, -1, 0, -1, 0]
                }));
                break;
              case 'Blur':
                object.filters.push(new fabric.Image.filters.Convolute({
                  matrix: [1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9, 1 / 9]
                }));
                break;
              default:
                object.filters.push(new fabric.Image.filters[filterName]);
                break;
              }
            object.applyFilters(canvas.renderAll.bind(canvas));
            break;
          case /frames/.test(filterSrc):
            object.strokeWidth = 10;

            switch (filterSrc) {
            case '/img/frames/1_thumb.png':
              object.stroke = 'gray';
              break;
            case '/img/frames/2_thumb.png':
              object.stroke = 'yellow';
              break;
            case '/img/frames/4_thumb.png':
              object.stroke = 'blue';
              break;
            default:
              object.stroke = 'transparent';
              object.strokeWidth = 0;
              break;
            }

            object.applyFilters(canvas.renderAll.bind(canvas));
            break;
          case /masks/.test(filterSrc):
            switch (filterSrc) {
            case '/img/masks/normal.png':
              object.clipTo = function(ctx) {
                var x = -this.width / 2,
                  y = -this.height / 2;

                ctx.beginPath();
                ctx.moveTo(x, y);
                ctx.lineTo(x + this.width, y);
                ctx.lineTo(x + this.width, y + this.height);
                ctx.lineTo(x, y + this.height);
                ctx.lineTo(x, y);
                ctx.closePath();
              };
              self.render();
              break;
            case '/img/masks/mask1.png':
              object.clipTo = function(ctx) {
                ctx.arc(0, 0, this.height / 2, 0, Math.PI * 2, true);
              };
              self.render();
              break;
            case '/img/masks/mask2.png':
              object.clipTo = function(ctx) {
                var radius = 20,
                  width = this.width,
                  height = this.height,
                  x = -width / 2,
                  y = -height / 2;

                ctx.beginPath();
                ctx.moveTo(x + radius, y);
                ctx.lineTo(x + width - radius, y);
                ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
                ctx.lineTo(x + width, y + height - radius);
                ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
                ctx.lineTo(x + radius, y + height);
                ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
                ctx.lineTo(x, y + radius);
                ctx.quadraticCurveTo(x, y, x + radius, y);
                ctx.closePath();
              };
              self.render();
              break;
            case '/img/masks/mask4.png':
              object.clipTo = function(ctx) {
                var width = this.width,
                  height = this.height,
                  x = -width / 2,
                  y = -height / 2;

                ctx.beginPath();
                ctx.moveTo(x + width / 2, y);
                ctx.lineTo(x + width, y + height / 4);
                ctx.lineTo(x + width, y + 3 * (height / 4));
                ctx.lineTo(x + width / 2, y + height);
                ctx.lineTo(x, y + 3 * (height / 4));
                ctx.lineTo(x, y + height / 4);
                ctx.lineTo(x + width / 2, y);
                ctx.closePath();
              };
              self.render();
              break;
            default:
              fabric.loadSVGFromURL(data.src, function(objects, options) {
                var transparent = 'rgba(255,255,255,0)',
                  sharp = fabric.util.groupSVGElements(objects, options);

                sharp.set({
                  left: 0,
                  top: 0,
                  originX: 'center',
                  originY: 'center'
                });

                if (sharp.isSameColor && sharp.isSameColor() || !sharp.paths) {
                  sharp.setFill(transparent);
                } else if (sharp.paths) {
                  for (var i = 0; i < sharp.paths.length; i++) {
                    sharp.paths[i].setFill(transparent);
                  }
                }
                var path = '';
                for (i = 0; i < sharp.paths.length; i++) {
                  path += sharp.paths[i].d;
                }

                if (!object.mask) {
                  object.mask = (new fabric.Path(path)).set({
                    left: 0,
                    top: 0,
                    minX: 1,
                    minY: 1,
                    originX: 'center',
                    originY: 'center',
                    scaleX: object.width / mask.width,
                    scaleY: object.height / mask.height
                  });
                }

                object.clipTo = function(ctx) {
                  var mask = this.mask;
                  if (!mask.render) {
                    mask = new fabric.Path(this.mask.path, this.mask);
                    this.mask = mask;
                  }
                  mask.render(ctx);
                };
                self.render();
              });
              break;
            }
            break;
          }
        };
        self.getCanvasPhotos = function() {
          return _.filter(canvas.getObjects(), function(item) {
            return item.mt === 2;
          });
        };
        self.setCanvas = function(newCanvas) {
          canvas = newCanvas;
          canvas.selection = self.canvasDefaults.selection;
        };
        self.setTextDefaults = function(textDefaults) {
          self.textDefaults = textDefaults;
        };
        self.setJSONExportProperties = function(JSONExportProperties) {
          self.JSONExportProperties = JSONExportProperties;
        };
        self.setCanvasBackgroundPattern = function(color) {
          self.canvasBackgroundColor = color;
          canvas.setBackgroundColor(color);
          self.render();
        };
        self.clearBackground = function() {
          self.setCanvasBackgroundPattern('#fff');
          self.setCanvasBackgroundImage(null);
          self.render();
        };
        self.setCanvasBackgroundImage = function(image) {
          canvas.setBackgroundColor('#fff');
          var imageSource = '';
          if (image) {
            if (image._element) {
              imageSource = image._element.src;
            } else if (typeof image === 'string') {
              imageSource = image;
            } else {
              imageSource = image.src;
            }
            fabric.Image.fromURL(imageSource, function(img) {
              img.set({
                width: canvas.width,
                height: canvas.height,
                originX: 'left',
                originY: 'top'
              });
              canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));
              self.canvasBackgroundImage = canvas.backgroundImage;
            });
          } else {
            canvas.backgroundImage = null;
            self.canvasBackgroundImage = canvas.backgroundImage;
          }
        };
        self.setCanvasWidth = function(width) {
          self.canvasWidth = width;
          canvas.setWidth(width);
        };
        self.setCanvasHeight = function(height) {
          self.canvasHeight = height;
          canvas.setHeight(height);
        };
        self.setCanvasSize = function(width, height) {
          self.stopContinuousRendering();
          self.canvasWidth = width;
          self.canvasOriginalWidth = width;
          canvas.originalWidth = width;
          canvas.setWidth(width);
          self.canvasHeight = height;
          self.canvasOriginalHeight = height;
          canvas.originalHeight = height;
          canvas.setHeight(height);
        };
        self.isLoading = function() {
          return self.isLoading;
        };
        self.deactivateAll = function() {
          canvas.deactivateAll();
          self.render();
        };
        self.clearCanvas = function() {
          canvas.clear();
        };
        self.drawPDF = function() {
          canvas.deactivateAll();
          return canvas.toDataURLWithMultiplier('jpeg', 1, 1);
        };
        self.addObjectToCanvas = function(object) {
          canvas.add(object);
          self.setObjectZoom(object);
          canvas.setActiveObject(object);
          object.bringToFront();
          self.render();
        };
        self.addClipart = function(filename, left, top) {
          fabric.Image.fromURL(filename, function(object) {
            object.id = self.runningIndex;
            self.runningIndex += 1;
            object.mt = 3;
            object.originX = 'center';
            object.originY = 'center';
            object.originalLeft = self.canvasOriginalWidth / 2;
            object.originalTop = self.canvasOriginalHeight / 2;
            object.left = left || object.originalLeft;
            object.top = top || object.originalTop;
            object.originalScaleX = 1;
            object.originalScaleY = 1;
            object.scaleX = 1;
            object.scaleY = 1;
            self.addObjectToCanvas(object);
          });
        };
        self.addPhoto = function(object) {
          canvas.add(object);
          self.render();
        };
        self.clearPhoto = function() {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }

          var url = (object.scaleY / object.scaleX) > 2.5 ? '/img/upload2.png' : '/img/upload.png';

          if (object.src === url) {
            return;
          }

          fabric.Image.fromURL(url, function(oImg) {
            oImg.id = object.id;
            oImg.src = url;
            oImg.originalScaleX = object.scaleX;
            oImg.originalScaleY = object.scaleY;
            oImg.scaleX = object.scaleX;
            oImg.scaleY = object.scaleY;
            oImg.angle = object.angle;
            oImg.left = object.left;
            oImg.top = object.top;
            oImg.width = object.width;
            oImg.height = object.height;
            oImg.mt = 5;
            oImg.opacity = .7;
            oImg.originX = 'center';
            oImg.originY = 'center';
            oImg.filters[0] = new fabric.Image.filters.SuperFilter;
            canvas.add(oImg);
            self.render();
          });
          canvas.remove(object);
        };
        self.insertRandom = function(counter, arrayImages) {
          var objectsList = canvas.getObjects();
          for (var i = objectsList.length - 1; i >= 0; i--) {
            if (objectsList[i].mt == 5 &&
                        (objectsList[i].src.match(/img\/upload/) != null || objectsList[i]._element &&
                        objectsList[i]._element.src.match(/img\/upload/))) {
              canvas.setActiveObject(objectsList[i]);
              var randomObj = arrayImages[Math.floor(Math.random() * counter)];
              self.insertPhoto(randomObj.src.replace(/(\.[\w\d_-]+)$/i, '_s' + '$1'));
            }
          }
        };
        self.loadLayout = function(placeholders) {
          var photos = _.filter(canvas.getObjects(), function(item) {
            return item.mt === 2;
          });

          var counter = 0;

          _.each(placeholders, function(item) {
            if (counter >= photos.length) {
              return false;
            }
            var maxWidth = item.width * item.scaleX;
            var maxHeight = item.height * item.scaleY;
            var result = self.scaleImage(photos[counter].originalW, photos[counter].originalH,
                        maxWidth, maxHeight, false);

            photos[counter].set({
              scaleX: item.scaleX,
              scaleY: item.scaleY,
              originalScaleX: item.originalScaleX,
              originalScaleY: item.originalScaleY,
              width: item.width,
              height: item.height,
              cx: Math.abs(result.scale * result.targetLeft),
              cy: Math.abs(result.scale * result.targetTop),
              cw: result.width > maxWidth ? photos[counter].originalW - 2 *
                        Math.abs(result.scale * result.targetLeft) : photos[counter].originalW,
              ch: result.height > maxHeight ? photos[counter].originalH - 2 *
                        Math.abs(result.scale * result.targetTop) : photos[counter].originalH,
              angle: item.angle,
              left: item.left,
              top: item.top,
              originalLeft: item.originalLeft,
              originalTop: item.originalTop
            });
            counter++;
          });
          canvas.removeItems(5);
          fabric.util.enlivenObjects(placeholders.slice(counter), function(objects) {
            objects.forEach(function(o) {
              self.setObjectZoom(o);
              canvas.add(o);
            });
            FabricCanvas.canvas.renderAll();
          });
          self.render();
        };
        self.insertPhoto = function(path) {
          var activeObject = canvas.getActiveObject();
          if (!activeObject) {
            return;
          }

          fabric.Image.fromURL(path.replace('_s', '_m'), function(o) {
            o.id = activeObject.id;
            o.originalScaleX = 1;
            o.originalScaleY = 1;
            o.scaleX = activeObject.scaleX;
            o.scaleY = activeObject.scaleY;
            o.angle = activeObject.angle;
            o.originalLeft = activeObject.originalLeft;
            o.originalTop = activeObject.originalTop;
            o.left = activeObject.left;
            o.top = activeObject.top;

            var maxWidth = activeObject.width * activeObject.scaleX;
            var maxHeight = activeObject.height * activeObject.scaleY;
            var result = self.scaleImage(o.width, o.height, maxWidth, maxHeight, false);

            o.cs = result.scale;
            o.cx = Math.abs(result.scale * result.targetLeft);
            o.cy = Math.abs(result.scale * result.targetTop);
            o.ch = result.height > maxHeight ? o.height - 2 * o.cy : o.height;
            o.cw = result.width > maxWidth ? o.width - 2 * o.cx : o.width;

            o.width = activeObject.width;
            o.height = activeObject.height;
            o.mt = 2;
            o.originX = 'center';
            o.originY = 'center';
            o.clipTo = activeObject.clipTo;
            o.filters[0] = new fabric.Image.filters.SuperFilter;

            o.setControlsVisibility({
              ml: false,
              mr: false,
              mt: false,
              mb: false
            });

            canvas.add(o);
            o.zoomBy(function() {
              canvas.renderAll();
            });
          });
          canvas.remove(activeObject);
        };

        self.addImage = function(item) {
          fabric.Image.fromURL(item.url, function(object) {
            object.id = self.runningIndex;
            self.runningIndex += 1;
            for (var p in item) {
              if (item.hasOwnProperty(p)) {
                object[p] = item[p];
              }
            }
            object.left = item.left * self.canvasScale;
            object.top = item.top * self.canvasScale;
            object.scaleX = item.scaleX * self.canvasScale;
            object.scaleY = item.scaleY * self.canvasScale;
            self.addObjectToCanvas(object);
          });
        };
        self.setSize = function(value) {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }

          if (object.paths) {
            object.scaleToWidth(object.width);
            object.scaleToHeight(object.height);
          } else {
            object.set({
              width: object.width * value,
              height: object.height * value
            });
          }
          self.render();
        };
        self.increaseAngle = function(value) {
          setActiveProp('angle', getActiveProp('angle') + value);
          self.render();
        };
        self.setAngle = function(value) {
          setActiveProp('angle', value);
          self.render();
        };
        self.deleteImage = function() {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }
          if (object.type === 'image' || object.paths) {
            canvas.remove(object);
            self.render();
          }
        };
        self.redEyesRemove = function() {
          var photo = canvas.getActiveObject();

          if (photo.clipTo) {
            return alert('Nie można zastosować efektu przy włączonej masce');
          }

          if (photo.left - photo.width * photo.scaleX / 2 < 0
                    || photo.left + photo.width * photo.scaleX / 2 > canvas.width
                    || photo.top - photo.height * photo.scaleY / 2 < 0
                    || photo.top + photo.height * photo.scaleY / 2 > canvas.height) {
            return alert('Obraz musi znajdować się wewnątrz obszaru edytowania');
          }

          if (photo.type !== 'image') {
            return;
          }

          var foundEyes = false;
          photo.bringToFront();

          var ctx = canvas.getContext('2d');
          var tracker = new tracking.ObjectTracker(['eye']);
          tracker.setStepSize(1.7);
          self.deactivateAll();
          tracker.on('track', function(event) {
            event.data.forEach(function(rect) {
              foundEyes = true;
              var imgData = ctx.getImageData(rect.x, rect.y, rect.width, rect.height);
              for (var i = 0; i < imgData.data.length; i += 4) {
                var redIntensity = imgData.data[i] / ((imgData.data[i + 1] + imgData.data[i + 2]) / 2);
                if (redIntensity > 1.5) {
                  imgData.data[i] = (imgData.data[i + 1] + imgData.data[i + 2]) / 2;
                }
              }
              ctx.putImageData(imgData, rect.x, rect.y);
            });
          });
          tracking.track('#' + ctx.canvas.id, tracker);

          if (foundEyes !== true) {
            canvas.setActiveObject(photo);
            return;
          }
          var activeData = ctx.getImageData(photo.left - photo.scaleX * photo.width / 2,
                    photo.top - photo.scaleY * photo.height / 2,
                    photo.width * photo.scaleX, photo.height * photo.scaleY);

          var newCanvas = document.createElement('canvas');
          newCanvas.width = photo.scaleX * photo.width;
          newCanvas.height = photo.scaleY * photo.height;

          newCanvas.getContext('2d').putImageData(activeData, 0, 0);

          var dURL = newCanvas.toDataURL('image/png');
          photo.scaleX = 1;
          photo.scaleY = 1;
          var img = new Image;
          img.onload = function() {
            photo.setElement(img);
            canvas.setActiveObject(photo);
            self.setFilter('contrast', 0);
          };
          img.src = dURL;
        };
        self.fitPhoto = function(props) {
          var object = canvas.getActiveObject(), params;

          if (props) {
            params = {
              cx: props.left * object.cs,
              cy: props.top * object.cs,
              ch: props.height * object.cs,
              cw: props.width * object.cs,
              width: props.width,
              height: props.height
            };
          } else {
            var result = self.scaleImage(object.originalW, object.originalH,
                canvas.width, canvas.height, false);

            params = {
              cx: 0,
              cy: 0,
              ch: object.originalH,
              cw: object.originalW,
              width: result.width,
              height: result.height
            };
          }

          if (!object && object.type !== 'image' && object.mt !== 2) {
            return;
          }

          object.set(params);
          object.zoomBy(function() {
            canvas.renderAll();
          });
        };
        self.clearGrid = function() {
          canvas.removeItems(4);
        };
        self.drawGrid = function() {
          var width = canvas.width,
            height = canvas.height,
            size = self.grid,
            line,
            offsetWidth = width % size > 0 ? Math.ceil((size - width % size) / 2) : 0,
            offsetHeight = height % size > 0 ? Math.ceil((size - height % size) / 2) : 0;

          for (var i = 1; i < Math.ceil(width / size); ++i) {
            line = new fabric.Line([i * size - offsetWidth, 0, i * size - offsetWidth, height], {
              stroke: '#999',
              opacity: .5,
              mt: 4,
              selectable: false
            });
            canvas.add(line);
            line.sendToBack();
          }
          for (i = 1; i < Math.ceil(height / size); ++i) {
            line = new fabric.Line([0, i * size - offsetHeight, width, i * size - offsetHeight], {
              stroke: '#999',
              opacity: .5,
              mt: 4,
              selectable: false
            });
            canvas.add(line);
            line.sendToBack();
          }
          canvas.renderAll();
        };
        self.redrawGrid = function() {
          if (self.grid <= 0) {
            return;
          }

          self.drawGrid();
        };
        self.zoomGrid = function() {
          if (self.gridOriginal <= 0) {
            return;
          }

          self.grid = self.gridOriginal * self.canvasScale;
        };
        self.fillShape = function(value) {
          var object = canvas.getActiveObject();
          if (object === null || object.type !== 'path-group') {
            return;
          }

          object.setFill(value);
          object.setTempFill = function(value) {
            for (var i in object.paths) {
              if (!object.paths.hasOwnProperty(i)) {
                return;
              }
              object.paths[i].setFill(value);
            }
          };
          object.setTempFill(value);
          self.render();
          object.saveState();// ?
        };
        self.fillLinearGradient = function(value1, value2) {
          var object = canvas.getActiveObject();
          if (object === null || object.type !== 'path-group') {
            return;
          }
          object.setFill(value1);
          object.setTempFill = function(value1) {
            for (var i in object.paths) {
              if (!object.paths.hasOwnProperty(i)) {
                return;
              }
              object.paths[i].setFill(value1);
            }
          };
          object.setTempFill(value1);
          object.setGradient('fill', {
            x1: 0,
            y1: object.height,
            x2: object.width,
            y2: object.height,
            colorStops: {
              0: value1,
              1: value2
            }
          });
          self.render();
        };
        self.fillCircularGradient = function(value1, value2) {
          var object = canvas.getActiveObject();
          if (object === null || object.type !== 'path-group') {
            return;
          }
          object.setFill(value1);
          object.setTempFill = function(value1) {
            for (var i in object.paths) {
              if (!object.paths.hasOwnProperty(i)) {
                return;
              }
              object.paths[i].setFill(value1);
            }
          };
          object.setTempFill(value1);
          var max = object.width > object.height ? object.width : object.height;
          object.setGradient('fill', {
            type: 'radial',
            x1: object.width / 2,
            y1: object.height / 2,
            x2: object.width / 2,
            y2: object.height / 2,
            r1: 0,
            r2: max * .9,
            colorStops: {
              0: value1,
              1: value2
            }
          });
          self.render();
        };
        self.setBackgroundImage = function(src) {
          fabric.Image.fromURL(src, function(img) {
            img.set({
              width: canvas.width,
              height: canvas.height,
              originX: 'left',
              originY: 'top'
            });
            canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));
          });
          return img;
        };
        self.setBorderWidth = function(value) {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }
          value = value < 0 ? 0 : (value > 150 ? 150 : value);

          for (var i in object.paths) {
            if (!object.paths.hasOwnProperty(i)) {
              return;
            }
            object.paths[i].set('strokeWidth', parseInt(value));
          }
          self.render();
        };
        self.setStrokeColor = function(value) {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }

          for (var i in object.paths) {
            if (!object.paths.hasOwnProperty(i)) {
              return;
            }
            object.paths[i].setStroke(value);
          }
          self.render();
        };
        self.getObject = function() {
          return canvas.getActiveObject();
        };
        self.createShape = function(src, left, top) {
          fabric.loadSVGFromURL(src, function(objects, options) {
            var shape = fabric.util.groupSVGElements(objects, options);
            shape.set({
              originalLeft: self.canvasOriginalWidth / 2,
              originalTop: canvas.height / 2,
              left: (left || (self.canvasOriginalWidth / 2)) * self.canvasScale,
              top: (top || (self.canvasOriginalHeight / 2)) * self.canvasScale,
              originX: 'center',
              originY: 'center',
              mt: 6,
              originalScaleX: 1,
              originalScaleY: 1,
              scaleX: self.canvasScale,
              scaleY: self.canvasScale
            });
            if (shape.isSameColor() || !shape.paths) {
              shape.setFill('#ffffff');
            } else if (shape.paths) {
              for (var i = 0; i < shape.paths.length; i++) {
                shape.paths[i].setFill('#ffffff');
              }
            }
            for (i = 0; i < shape.paths.length; i++) {
              shape.paths[i].strokeDashArray = null;
            }
            canvas.add(shape);
            canvas.setActiveObject(shape);
            self.render();
          });
        };
        self.addShape = function(svgURL) {
          fabric.loadSVGFromURL(svgURL, function(objects) {
            var object = fabric.util.groupSVGElements(objects, self.shapeDefaults);
            object.id = self.runningIndex;
            self.runningIndex += 1;

            for (var p in self.shapeDefaults) {
              object[p] = self.shapeDefaults[p];
            }

            if (object.isSameColor() || !object.paths) {
              object.setFill('#0088cc');
            } else if (object.paths) {
              for (var i = 0; i < object.paths.length; i++) {
                object.paths[i].setFill('#0088cc');
              }
            }
            self.addObjectToCanvas(object);
          });
        };
        self.applyToAll = function() {
          var object = canvas.getActiveObject();
          if (!object || object.type !== 'textbox') {
            return;
          }

          var style = {
            fontSize: parseInt(self.getFontSize(), 10),
            fontFamily: self.getFontFamily().toLowerCase(),
            lineHeight: parseFloat(self.getLineHeight()),
            fontWeight: self.isBold() ? 'bold' : '',
            fontStyle: self.isItalic() ? 'italic' : '',
            textDecoration: (self.isUnderline() ? 'underline' : '') + (self.isLinethrough() ? ' line-through' : ''),
            textAlign: self.getTextAlign(),
            fill: self.getFill()
          };

          for (var i = 0, objects = canvas.getObjects(); i < objects.length; i += 1) {
            if (objects[i].type !== 'textbox') {
              return;
            }

            objects[i].set({
              fontSize: fontSize,
              fontFamily: style.fontFamily
            });

            setActiveStyle('lineHeight', style.lineHeight, objects[i]);
            setActiveStyle('fontWeight', style.fontWeight, objects[i]);
            setActiveStyle('fontStyle', style.fontStyle, objects[i]);
            setActiveStyle('textDecoration', style.textDecoration, objects[i]);
            setActiveStyle('textAlign', style.textAlign, objects[i]);
            setActiveStyle('fill', style.fill, objects[i]);
          }
          canvas.deactivateAll();
          self.render();
        };

        self.drawPhoto = function() {
          fabric.Image.fromURL('/img/upload.png', function(o) {
            o.id = self.createId();
            o.src = '/img/upload.png';
            o.originalScaleX = 1;
            o.originalScaleY = 1;
            o.scaleX = self.canvasScale;
            o.scaleY = self.canvasScale;
            o.originalLeft = self.canvasOriginalWidth / 2;
            o.originalTop = self.canvasOriginalHeight / 4;
            o.left = o.originalLeft * self.canvasScale;
            o.top = o.originalTop * self.canvasScale;
            o.width = 400;
            o.height = 250;
            o.mt = 5;
            o.opacity = .7;
            o.originX = 'center';
            o.originY = 'center';
            o.filters[0] = new fabric.Image.filters.SuperFilter({
              type: 'SuperFilter',
              brightness: 0,
              contrast: 0
            });
            canvas.add(o);
            canvas.setActiveObject(o);
            self.render();
          });
        };

        self.drawText = function() {
          var t = new FabricWindow.Textbox('Wprowadź tekst', {
            left: self.canvasOriginalWidth / 2,
            top: self.canvasOriginalHeight / 6,
            width: 300,
            originX: 'center',
            originY: 'center',
            id: self.createId()
          });

          canvas.add(t);
          self.setObjectZoom(t);
          canvas.setActiveObject(t);
          t.bringToFront();
          self.render();
        };
        self.deleteText = function() {
          var object = canvas.getActiveObject();
          if (!object || object.type !== 'textbox') {
            return;
          }
          canvas.remove(object);
          self.render();
        };
        self.getFontSize = function() {
          return getActiveStyle('fontSize');
        };
        self.setFontSize = function(value, execute) {
          execute = typeof execute !== 'undefined' ? execute : 1;
          if (execute === 0) {
            return;
          }
          setActiveStyle('fontSize', parseInt(value, 10));
          self.render();
        };
        self.getFontFamily = function() {
          var fontFamily = getActiveProp('fontFamily');
          return fontFamily ? fontFamily.toLowerCase() : '';
        };
        self.setFontFamily = function(value, execute) {
          execute = typeof execute !== 'undefined' ? execute : 1;
          if (execute === 0) {
            return;
          }
          setActiveProp('fontFamily', value.toLowerCase());
          self.render();
        };
        self.getLineHeight = function() {
          return getActiveStyle('lineHeight');
        };
        self.setLineHeight = function(value, execute) {
          execute = typeof execute !== 'undefined' ? execute : 1;
          if (execute === 0) {
            return;
          }
          setActiveStyle('lineHeight', parseFloat(value));
          self.render();
        };
        self.isBold = function() {
          return getActiveStyle('fontWeight') === 'bold';
        };
        self.toggleBold = function() {
          setActiveStyle('fontWeight', getActiveStyle('fontWeight') === 'bold' ? '' : 'bold');
          self.render();
        };
        self.isItalic = function() {
          return getActiveStyle('fontStyle') === 'italic';
        };
        self.toggleItalic = function() {
          setActiveStyle('fontStyle', getActiveStyle('fontStyle') === 'italic' ? '' : 'italic');
          self.render();
        };
        self.isUnderline = function() {
          return getActiveStyle('textDecoration').indexOf('underline') > -1;
        };
        self.toggleUnderline = function() {
          var value = self.isUnderline() ? getActiveStyle('textDecoration').replace('underline', '')
                    : getActiveStyle('textDecoration') + ' underline';
          setActiveStyle('textDecoration', value);
          self.render();
        };
        self.isLinethrough = function() {
          return getActiveStyle('textDecoration').indexOf('line-through') > -1;
        };
        self.toggleLinethrough = function() {
          var value = self.isLinethrough() ? getActiveStyle('textDecoration').replace('line-through', '')
                    : getActiveStyle('textDecoration') + ' line-through';
          setActiveStyle('textDecoration', value);
          self.render();
        };
        self.getTextAlign = function() {
          return getActiveProp('textAlign');
        };
        self.setTextAlign = function(value) {
          setActiveProp('textAlign', value);
          self.render();
        };
        self.getOpacity = function() {
          return getActiveStyle('opacity');
        };
        self.setOpacity = function(value) {
          setActiveStyle('opacity', value);
          self.render();
        };
        self.getFlipX = function() {
          return getActiveProp('flipX');
        };
        self.setFlipX = function(value) {
          setActiveProp('flipX', value);
        };
        self.toggleFlipX = function() {
          if (!canvas.getActiveObject()) {
            return;
          }
          var value = self.getFlipX() ? false : true;
          self.setFlipX(value);
          self.render();
        };
        self.getFlipY = function() {
          return getActiveProp('flipY');
        };
        self.setFlipY = function(value) {
          setActiveProp('flipY', value);
        };
        self.toggleFlipY = function() {
          if (!canvas.getActiveObject()) {
            return;
          }
          var value = self.getFlipY() ? false : true;
          self.setFlipY(value);
          self.render();
        };
        self.centerH = function() {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }
          object.centerH();
          self.updateActiveObjectOriginals();
          self.render();
        };
        self.centerV = function() {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }
          object.centerV();
          self.updateActiveObjectOriginals();
          self.render();
        };
        self.centerTop = function() {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }
          if (object.paths) {
            object.top = object.height / 2 * object.scaleY + 8 * self.canvasScale;
          } else {
            object.top = object.originY === 'center' ? object.height * self.canvasScale / 2 : 0;
          }

          object.originalScaleX = object.scaleX / self.canvasScale;
          object.originalScaleY = object.scaleY / self.canvasScale;
          object.originalLeft = object.left / self.canvasScale;
          object.originalTop = object.top / self.canvasScale;
          self.render();
        };
        self.centerBottom = function() {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }
          if (object.paths) {
            object.top = self.canvasHeight - object.left / 2 * object.scaleY - 20 * self.canvasScale;
          } else if (object.originY === 'center') {
            object.top = self.canvasHeight - object.height * self.canvasScale / 2;
          } else {
            object.top = self.canvasHeight - object.height * self.canvasScale;
          }

          object.originalScaleX = object.scaleX / self.canvasScale;
          object.originalScaleY = object.scaleY / self.canvasScale;
          object.originalLeft = object.left / self.canvasScale;
          object.originalTop = object.top / self.canvasScale;
          self.render();
        };
        self.centerLeft = function() {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }
          if (object.paths) {
            object.left = object.left / 2 * object.scaleX;
          } else {
            object.left = object.originX === 'center' ? object.width * self.canvasScale / 2 : 0;
          }

          object.originalScaleX = object.scaleX / self.canvasScale;
          object.originalScaleY = object.scaleY / self.canvasScale;
          object.originalLeft = object.left / self.canvasScale;
          object.originalTop = object.top / self.canvasScale;
          self.render();
        };
        self.centerRight = function() {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }
          if (object.paths) {
            object.left = self.canvasWidth - object.left / 2 * object.scaleX - 20 * self.canvasScale;
          } else if (object.originX === 'center') {
            object.left = self.canvasWidth - object.width * self.canvasScale / 2;
          } else {
            object.left = self.canvasWidth - object.width * self.canvasScale;
          }

          object.originalScaleX = object.scaleX / self.canvasScale;
          object.originalScaleY = object.scaleY / self.canvasScale;
          object.originalLeft = object.left / self.canvasScale;
          object.originalTop = object.top / self.canvasScale;
          self.render();
        };
        self.sendBackwards = function() {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }
          canvas.sendBackwards(object, true);
          self.render();
        };
        self.sendToBack = function() {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }
          canvas.sendToBack(object);
          self.render();
        };
        self.bringForward = function() {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }
          canvas.bringForward(object, true);
          self.render();
        };
        self.bringToFront = function() {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }
          canvas.bringToFront(object);
          self.render();
        };
        self.setFilter = function(name, value) {
          var object = canvas.getActiveObject();
          if (!object || object.mt !== 2) {
            return;
          }

          object.filters[0][name] = value;
          object.applyFilters(canvas.renderAll.bind(canvas));
          self.render();
        };
        self.isTinted = function() {
          return getActiveProp('isTinted');
        };
        self.toggleTint = function() {
          var object = canvas.getActiveObject();
          object.isTinted = !object.isTinted;
          object.filters[0].opacity = object.isTinted ? 1 : 0;
          object.applyFilters(canvas.renderAll.bind(canvas));
        };
        self.getTint = function() {
          var object = canvas.getActiveObject();
          if (!object) {
            return '';
          }
          if (object.filters && object.filters[0]) {
            return object.filters[0].color;
          }
        };
        self.setTint = function(tint) {
          if (!isHex(tint)) {
            return;
          }
          var object = canvas.getActiveObject();
          if (object.filters && object.filters[0]) {
            object.filters[0].color = tint;
            object.applyFilters(canvas.renderAll.bind(canvas));
          }
        };
        self.getFill = function() {
          return getActiveStyle('fill');
        };
        self.setFill = function(value) {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }
          if (object.type === 'textbox' || object.type === 'text') {
            setActiveStyle('fill', '#' + value);
            self.render();
          } else {
            self.setFillPath(object, '#' + value);
          }
        };
        self.setFillPath = function(object, value) {
          if (object.isSameColor() || !object.paths) {
            return object.setFill(value);
          }
          if (object.paths) {
            for (var i = 0; i < object.paths.length; i++) {
              object.paths[i].setFill(value);
            }
          }
        };
        self.openCropTool = function() {
          var object = canvas.getActiveObject();

          if (!object) {
            return;
          }

          $rootScope.$broadcast('image:crop', {
            src: object.orgSrc,
            width: object.originalW,
            height: object.originalH,
            cs: object.cs,
            cw: object.cw,
            ch: object.ch,
            cx: object.cx,
            cy: object.cy
          });
        };
        self.copy = function() {
          clipboard = canvas.getActiveObject();
        };
        self.paste = function() {
          if (!clipboard) {
            return;
          }
          var copy = fabric.util.object.clone(clipboard);
          if (cutting) {
            clipboard = false;
            cutting = false;
          } else {
            clipboard = copy;
            copy.top += 20;
            copy.left += 20;
          }
          canvas.add(copy);
          self.setObjectZoom(copy);
          canvas.setActiveObject(copy);
          copy.bringToFront();
          self.render();
        };
        self.cut = function() {
          clipboard = canvas.getActiveObject();
          if (!clipboard) {
            return;
          }
          cutting = true;
          self.deleteActiveObject();
        };
        self.resetZoom = function() {
          self.canvasScale = 1;
          self.setZoom();
        };
        self.setZoom = function() {
          self.clearGrid();
          self.zoomGrid();
          var objects = canvas.getObjects();
          for (var i = 0; i < objects.length; i++) {
            self.setObjectZoom(objects[i]);
          }
          self.setCanvasZoom();
          self.redrawGrid();
        };
        self.setObjectZoom = function(o) {
          o.originalScaleX = o.originalScaleX ? o.originalScaleX : o.scaleX;
          o.originalScaleY = o.originalScaleY ? o.originalScaleY : o.scaleY;
          o.originalLeft = o.originalLeft ? o.originalLeft : o.left;
          o.originalTop = o.originalTop ? o.originalTop : o.top;
          o.scaleX = o.originalScaleX * self.canvasScale;
          o.scaleY = o.originalScaleY * self.canvasScale;
          o.left = o.originalLeft * self.canvasScale;
          o.top = o.originalTop * self.canvasScale;
          o.setCoords();
        };
        self.setCanvasZoom = function() {
          var tempWidth = self.canvasOriginalWidth * self.canvasScale;
          var tempHeight = self.canvasOriginalHeight * self.canvasScale;

          self.canvasWidth = tempWidth;
          self.canvasHeight = tempHeight;
          canvas.setWidth(tempWidth);
          canvas.setHeight(tempHeight);
        };
        self.updateActiveObjectOriginals = function() {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }
          object.originalScaleX = object.scaleX / self.canvasScale;
          object.originalScaleY = object.scaleY / self.canvasScale;
          object.originalLeft = object.left / self.canvasScale;
          object.originalTop = object.top / self.canvasScale;
        };
        self.toggleLockActiveObject = function() {
          var object = canvas.getActiveObject();
          if (!object) {
            return;
          }
          object.lockMovementX = !object.lockMovementX;
          object.lockMovementY = !object.lockMovementY;
          object.lockScalingX = !object.lockScalingX;
          object.lockScalingY = !object.lockScalingY;
          object.lockUniScaling = !object.lockUniScaling;
          object.lockRotation = !object.lockRotation;
          object.lockObject = !object.lockObject;
          self.render();
        };
        self.deleteActiveObject = function() {
          canvas.remove(canvas.getActiveObject());
          self.render();
        };
        self.isLoading = function() {
          return self.loading;
        };
        self.setLoading = function(value) {
          self.loading = value;
        };
        self.setDirty = function(value) {
          FabricDirtyStatus.setDirty(value);
        };
        self.isDirty = function() {
          return FabricDirtyStatus.isDirty();
        };
        self.setInitalized = function(value) {
          self.initialized = value;
        };
        self.isInitalized = function() {
          return self.initialized;
        };
        self.getJSON = function() {
          var initialCanvasScale = self.canvasScale;
          self.canvasScale = 1;
          self.resetZoom();
          canvas.stateful = true;
          var json = JSON.stringify(canvas.toJSON(self.JSONExportProperties));
          self.canvasScale = initialCanvasScale;
          self.setZoom();
          return json;
        };
        self.loadJSON = function(json) {
          var startTime = (new Date()).getTime();
          self.setLoading(true);
          canvas.loadFromJSON(json, function() {
            $timeout(function() {
              self.setLoading(false);
              self.setCanvasBackgroundPattern(json.background || '#fff');
              self.setCanvasBackgroundImage(json.backgroundImg || '');

              console.log('Render time: ', (new Date()).getTime() - startTime);

              if (self.grid > 0) {
                self.drawGrid();
              }

              self.setZoom();
            });
          });
        };
        self.getCanvasData = function() {
          return canvas.toDataURL({
            width: canvas.getWidth(),
            height: canvas.getHeight(),
            multiplier: self.downloadMultiplier
          });
        };
        self.getCanvasBlob = function() {
          var base64Data = self.getCanvasData(),
            data = base64Data.replace('data:image/png;base64,', ''),
            blob = b64toBlob(data, 'image/png');
          return URL.createObjectURL(blob);
        };
        self.download = function(name) {
          self.deactivateAll();
          var link = document.createElement('a');
          link.download = name + '.png';
          link.href = self.getCanvasBlob();
          link.click();
        };
        self.continuousRenderCounter = 0;
        self.stopContinuousRendering = function() {
          $timeout.cancel(self.continuousRenderHandle);
          self.continuousRenderCounter = self.maxContinuousRenderLoops;
        };
        self.startContinuousRendering = function() {
          self.continuousRenderCounter = 0;
          self.continuousRender();
        };
        self.continuousRender = function() {
          if (self.userHasClickedCanvas || self.continuousRenderCounter > self.maxContinuousRenderLoops) {
            return;
          }
          self.continuousRenderHandle = $timeout(function() {
            self.setZoom();
            self.render();
            self.continuousRenderCounter++;
            self.continuousRender();
          }, self.continuousRenderTimeDelay);
        };
        self.setUserHasClickedCanvas = function(value) {
          self.userHasClickedCanvas = value;
        };
        self.createId = function() {
          return Math.floor(Math.random() * 1E4);
        };
        self.setCanvasDefaults = function() {
          canvas.selection = self.canvasDefaults.selection;
          canvas.renderOnAddRemove = self.canvasDefaults.renderOnAddRemove;
        };
        self.setWindowDefaults = function() {
          FabricWindow.Object.prototype.borderColor = self.windowDefaults.borderColor;
          FabricWindow.Object.prototype.cornerColor = self.windowDefaults.cornerColor;
          FabricWindow.Object.prototype.cornerSize = self.windowDefaults.cornerSize;
          FabricWindow.Object.prototype.transparentCorners = self.windowDefaults.transparentCorners;
          FabricWindow.Object.prototype.rotatingPointOffset = self.windowDefaults.rotatingPointOffset;
        };

        self.onDragEnd = function(e) {
          if (!self.dragInit) {
            self.dragHolder = null;
            return;
          }

          self.dragInit = false;

          var doc = document.documentElement,
            left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0),
            top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0),

            offset = $('canvas').offset(),
            posX = left + e.clientX - offset.left,
            posY = top + e.clientY - offset.top;

          if (posX < 0 || posY < 0) {
            return;
          }

          if (/cliparts/.test(self.dragHolder)) {
            return self.addClipart(self.dragHolder, posX, posY);
          }

          if (/shapes/.test(self.dragHolder)) {
            return self.createShape(self.dragHolder, posX, posY);
          }

          if (!/uploads/.test(self.dragHolder)) {
            return;
          }

          for (var i = 0, objects = canvas.getObjects(); i < objects.length; i++) {
            if (objects[i].mt !== 5) {
              continue;
            }

            if (posX > (objects[i].left - objects[i].width / 2)
                && posX < (objects[i].left - objects[i].width / 2) + objects[i].width
                && posY > (objects[i].top - objects[i].height / 2)
                && posY < (objects[i].top - objects[i].height / 2) + objects[i].height) {
              canvas.setActiveObject(objects[i]);
              return self.insertPhoto(self.dragHolder);
            }
          }

          fabric.Image.fromURL(self.dragHolder.replace(/(\.[\w\d_-]+)$/i, '_m' + '$1'), function(oImg) {
            oImg.id = self.createId();
            oImg.originalScaleX = 1;
            oImg.originalScaleY = 1;
            oImg.scaleX = self.canvasScale;
            oImg.scaleY = self.canvasScale;
            oImg.originalLeft = (posX / self.canvasScale);
            oImg.originalTop = (posY / self.canvasScale);
            oImg.left = posX;
            oImg.top = posY;

            var result = self.scaleImage(oImg.width, oImg.height, 400, 250, false);

            oImg.cx = Math.abs(result.scale * result.targetLeft);
            oImg.cy = Math.abs(result.scale * result.targetTop);
            oImg.ch = oImg.height;
            oImg.cw = oImg.width;

            oImg.width = result.width;
            oImg.height = result.height;
            oImg.mt = 2;
            oImg.originX = 'center';
            oImg.originY = 'center';
            oImg.filters[0] = new fabric.Image.filters.SuperFilter;
            canvas.add(oImg);
            self.render();
          });
        };

        self.scaleImage = function(srcWidth, srcHeight, targetWidth, targetHeight, fLetterBox) {
          var result = {
            width: 0,
            height: 0,
            scale: 0,
            fScaleToTargetWidth: true
          };

          if (srcWidth <= 0 || srcHeight <= 0 || targetWidth <= 0 || targetHeight <= 0) {
            return result;
          }

          var scaleX1 = targetWidth,
            scaleY1 = (srcHeight * targetWidth) / srcWidth,
            scaleX2 = (srcWidth * targetHeight) / srcHeight,
            scaleY2 = targetHeight,
            fScaleOnWidth = (scaleX2 > targetWidth) ? fLetterBox : !fLetterBox;

          if (fScaleOnWidth) {
            result.scale = srcWidth / scaleX1;
            result.width = scaleX1;
            result.height = scaleY1;
            result.fScaleToTargetWidth = true;
          } else {
            result.scale = srcWidth / scaleX2;
            result.width = scaleX2;
            result.height = scaleY2;
            result.fScaleToTargetWidth = false;
          }
          result.targetLeft = (targetWidth - result.width) / 2;
          result.targetTop = (targetHeight - result.height) / 2;

          return result;
        };

        self.startCanvasListeners = function() {
          document.body.addEventListener('dragend', function(e) {
            self.onDragEnd(e);
          }, false);

          document.body.addEventListener('keydown', function(e) {
            var saveChange = false;
            switch (e.which) {
            case 46: // del
              var activeGroup = canvas.getActiveGroup(),
                object = canvas.getActiveObject();
              saveChange = object || activeGroup;

              if (activeGroup) {
                canvas.getActiveGroup().forEachObject(function(o) {
                  canvas.remove(o);
                });
                canvas.discardActiveGroup();
              }

              canvas.remove(object);
              break;
            case 65: // a
              if (e.ctrlKey) {
                e.preventDefault();
                e.stopPropagation();
                canvas.setActiveGroup(new fabric.Group(canvas.getObjects())).renderAll();
              }
              break;
            case 67: // c
              if (e.ctrlKey) {
                self.copy();
              }
              break;
            case 86: // v
              if (e.ctrlKey) {
                self.paste();
              }
              break;
            case 88: // x
              if (e.ctrlKey) {
                self.cut();
              }
              break;
            case 13: // enter
              var object = canvas.getActiveObject();
              if (!object || object.mt !== 2) {
                return;
              }

              var modalWindow = $('[modal-window]');

              if (modalWindow.length > 0 && window.cropper) {
                modalWindow.click();
                console.log(window.cropper.getData());
                self.fitPhoto(window.cropper.getCropBoxData());
              } else {
                self.openCropTool();
              }
              break;
            }

            if (saveChange) {
              self.render();
            }
          });

          fabric.util.addListener(canvas.upperCanvasEl, 'dblclick', function() {
            self.stopContinuousRendering();
            $timeout(function() {
              var object = canvas.getActiveObject() || {};

              switch (object.type) {
              case 'image':
                if (object.mt === 2) {
                  self.openCropTool();
                } else if (object.mt === 5) {
                  $rootScope.$broadcast('image:insert');
                }

                $location.path('/obraz');
                break;
              case 'textbox':
                $location.path('/tekst');
                break;
              }
            });
          });

          canvas.on('object:moving', function(e) {
            if (!self.dragToGrid || !self.grid || self.grid <= 0) {
              return false;
            }

            var grid = self.grid,
              width = canvas.width % grid,
              height = canvas.height % grid,
              offsetWidth = width > 0 ? Math.ceil((grid - width) / 2) : 0,
              offsetHeight = height > 0 ? Math.ceil((grid - height) / 2) : 0;

            e.target.set({
              left: Math.round(e.target.left / grid) * grid,
              top: Math.round(e.target.top / grid) * grid
            });

            if (e.target.type === 'textbox') {
              return false;
            }

            if (e.target.width * e.target.scaleX % grid !== 0) {
              e.target.set({
                left: e.target.left + (e.target.width * e.target.scaleX / 2 % grid - offsetWidth)
              });
            }

            if (e.target.height * e.target.scaleY % grid !== 0) {
              e.target.set({
                top: e.target.top + (e.target.height * e.target.scaleY / 2 % grid - offsetHeight)
              });
            }
          });
          canvas.on('object:selected', function(e) {
            self.stopContinuousRendering();
            $timeout(function() {
              var object = e.target;

              if (!object) {
                return false;
              }

              self.previousSelected = object;

              switch (object.type) {
              case 'textbox':
                $('#text-tab').show();
                $('#image-tab').hide();

                $rootScope.$broadcast('text:selected', {
                  font: capitalizeWords(self.getFontFamily()),
                  fontSize: self.getFontSize(),
                  lineHeight: self.getLineHeight().toFixed(1),
                  color: self.getFill()
                });
                break;
              default:
                $('#image-tab').show();
                $('#text-tab').hide();

                $rootScope.$broadcast('object:selected', {
                  contrast: object.filters[0] ? object.filters[0].contrast : 0,
                  brightness: object.filters[0] ? object.filters[0].brightness : 0,
                  opacity: object.opacity,
                  disable: object.mt !== 2
                });

                break;
              }

              $rootScope.$broadcast('object:rotating', {
                angle: object.angle
              });

              self.setDirty(true);
            });
          });
          canvas.on('selection:created', function() {
            self.stopContinuousRendering();
          });
          canvas.on('selection:cleared', function() {
            $timeout(function() {
              $rootScope.$broadcast('object:deselected');
              self.previousSelected = null;
              $('#image-tab, #text-tab').hide();
            });
          });
          canvas.on('after:render', function() {
            canvas.calcOffset();
          });
          canvas.on('object:modified', function() {
            self.stopContinuousRendering();
            $timeout(function() {
              self.updateActiveObjectOriginals();
              self.setDirty(true);
            });
          });
        };

        self.undo = function() {
          self.currentChange = self.currentChange + 1;
          $rootScope.$broadcast('history:load');
        };

        self.redo = function() {
          self.currentChange = self.currentChange - 1;
          $rootScope.$broadcast('history:load');
        };

        self.init = function() {
          canvas = FabricCanvas.getCanvas();
          self.canvasId = FabricCanvas.getCanvasId();
          JSONObject = angular.fromJson(self.json) || {};
          self.canvasScale = 1;
          JSONObject.background = JSONObject.background || '#ffffff';
          self.setCanvasBackgroundPattern(JSONObject.background);
          JSONObject.backgroundImg = JSONObject.backgroundImg || '';
          self.setCanvasBackgroundImage(JSONObject.backgroundImg);
          self.setDirty(false);
          self.setInitalized(true);
          self.setCanvasDefaults();
          self.setWindowDefaults();
          self.startCanvasListeners();
          FabricDirtyStatus.startListening();
        };
        self.init();
        return self;
      };
    }
  ]);
'use strict';
angular.module('common.fabric.canvas', ['common.fabric.window']).service('FabricCanvas',
  ['FabricWindow', '$rootScope', function(FabricWindow, $rootScope) {
    var self = {
      canvasId: null,
      element: null,
      canvas: null
    };

        /**
         * create id
         * @return {number}
         */
    function createId() {
      return Math.floor(Math.random() * 1E4);
    }

    self.setElement = function(element) {
      self.element = element;
      $rootScope.$broadcast('canvas:element:selected');
    };
    self.createCanvasWithoutBroadCast = function() {
      self.canvasId = 'fabric-canvas-' + createId();
      self.element.attr('id', self.canvasId);
      self.canvas = new FabricWindow.Canvas(self.canvasId);
      return self.canvas;
    };
    self.createCanvas = function() {
      self.canvasId = 'fabric-canvas-' + createId();
      self.element.attr('id', self.canvasId);
      self.canvas = new FabricWindow.Canvas(self.canvasId);
      $rootScope.$broadcast('canvas:created');
      return self.canvas;
    };
    self.createStaticCanvas = function(id) {
      self.canvasId = id;
      self.element.attr('id', self.canvasId);
      self.canvas = new FabricWindow.StaticCanvas(self.canvasId);
      self.canvas.selection = false;
      return self.canvas;
    };
    self.getCanvas = function() {
      return self.canvas;
    };
    self.getCanvasId = function() {
      return self.canvasId;
    };
    return self;
  }]);
angular.module('common.fabric.window', []).factory('FabricWindow', ['$window', function($window) {
  return $window.fabric;
}]);
'use strict';
angular.module('common.fabric.constants', []).service('FabricConstants', [function() {
  var objectDefaults = {
    rotatingPointOffset: 20,
    padding: 0,
    borderColor: 'green',
    cornerColor: 'rgba(64, 159, 221, 1)',
    cornerSize: 8,
    transparentCorners: false,
    hasRotatingPoint: true,
    centerTransform: true
  };
  return {
    holidays: [[1, 6], [], [], [], [1, 3], [], [], [15], [], [], [1, 11], [25, 26]],
    labels: [['Nowy Rok', 'Izydora, Makarego', 'Danuty, Genowefy', 'Anieli, Eugeniusza', 'Szymona, Edwarda',
      'Trzech króli', 'Juliana, Lucjana', 'Seweryna, Juliusza', 'Weroniki, Juliana', 'Wilhelma, Jana',
      'Honoraty, Matyldy', 'Benedykta, Arkadiusza', 'Bogumiły, Weroniki', 'Feliksa, Domosława', 'Pawła, Izydora',
      'Marcelego, Włodzimierza', 'Antoniego, Rościsława', 'Piotra, Małgorzaty', 'Henryka, Mariusza',
      'Fabiana, Sebastiana', 'Agnieszki, Jarosława\nDzień babci', 'Anastazego, Wincentego\nDzień dziadka',
      'Ildefonsa, Rajmunda', 'Felicji, Tymoteusza', 'Pawła, Miłosza', 'Michała, Tytusa', 'Przybysława, Anieli',
      'Walerego, Radomira', 'Zdzisława, Józefa', 'Macieja, Martyny', 'Marceliny, Jana'], ['Brygidy, Seweryna',
        '  Marii, Mirosława,\nOfiarowanie Pańskie', 'Błażeja, Hipolita', 'Andrzeja, Weroniki', 'Agaty, Adelajdy',
        'Bogdana, Pawła', 'Ryszarda, Teodora', 'Hieronima, Ireny', 'Apolonii, Eryki', 'Elwiry, Jacka',
        'Lucjana, Olgierda', 'Radosława, Modesta', 'Grzegorza, Katarzyny',
        'Walentego, Metodego,\nWalentynki', 'Jowity, Faustyna', 'Danuty, Daniela', 'Zbigniewa, Lukasza',
        'Symeona, Konstancji', 'Arnolda, Konrada', 'Leona, Ludomira', 'Eleonory, Roberta', 'Małgorzaty, Piotra',
        'Romany, Damiana', 'Macieja, Bogusza', 'Wiktora, Cezarego', 'Mirosława, Aleksandra', 'Gabriela, Anastazji',
        'Romana, Lecha', 'Lecha, Lutosława'], ['Antoniny, Dawida', 'Heleny, Pawła', 'Maryny, Kunegundy',
          'Kazimierza, Eugeniusza', 'Adriana, Fryderyka', 'Róży, Agnieszki',
          'Tomasza, Felicyty', 'Wincentego, Beaty,\nDzień Kobiet', 'Franciszki, Brunona',
          'Cypriana, Marcelelego,\nDzień Mężczyzn', 'Konstantyna, Benedykta', 'Grzegorza, Justyna',
          'Bożeny, Krystyny', 'Leona, Matyldy', 'Klemensa, Ludwiki', 'Izabeli, Hilarego', 'Patryka, Zbigniewa',
          'Cyryla, Boguchwały', 'Józefa, Bogdana', 'Klaudii, Maurycego', 'Lubomira, Benedykta',
          'Katarzyny, Bogusława', 'Oktawiana, Feliksa', 'Marka, Katarzyny', 'Marioli, Wieczysława',
          'Larysy, Teodora', 'Lidii, Ernesta', 'Anieli, Jana', 'Wiktora, Eustachego', 'Anieli, Leonarda',
          'Beniamina, Leonarda'], ['Grażyny, Ireny,\n Prima Aprilis', 'Władysława, Franciszka',
            'Ryszarda, Pankracego', 'Izydora, Wacława', 'Ireny, Wincentego', 'Izoldy, Wilhelma', 'Rufina, Jana',
            'Cezaryny, Julii', 'Marii, Dymitra', 'Michała, Makarego', 'Filipa, Leona',
            'Juliusza, Zenona,\nŚwięto Miłosierdzia Bożego', 'Przemysława, Marcina',
            'Waleriana, Justyny', 'Ludwiny, Wacławy', 'Cecylii, Bernardety', 'Rudolfa, Roberta',
            'Bogusławy, Apoloniusza', 'Adolfa, Leona', 'Agnieszki, Mariana', 'Bartosza, Feliksa',
            'Leona, Łukasza,\nDzień Ziemii', 'Jerzego, Wojciecha', 'Feliksa, Grzegorza', 'Marka, Jarosława',
            'Marzeny, Marii', 'Teofila, Felicji', 'Piotra, Walerii', 'Katarzyny, Bogusława', 'Mariana, Donaty'],
    ['Międzynarodowe\nŚwięto Pracy', 'Zygmunta, Atanazego,\nDzień Flagi RP',
      'Święto Konstytucji\n3 Maja', 'Moniki, Floriana', 'Ireny, Waldemara', 'Judyty, Jakuba',
      'Ludmiły, Benedykta', 'Stanisława, Wiktora', 'Grzegorza, Karoliny', 'Izydora, Antoniny',
      'Igi, Władysławy', 'Pankracego, Dominika', 'Serwacego, Roberta', 'Bonifacego, Macieja',
      'Zofii, Izydora', 'Andrzeja, Szymona', 'Sławomira, Weroniki', 'Eryka, Jana', 'Piotra, Celestyna',
      'Bernardyna, Aleksandra', 'Wiktora, Tymoteusza', 'Heleny, Wiesławy', 'Iwony, Kryspina',
      'Joanny, Zuzanny', 'Grzegorza, Magdaleny', 'Filipa, Pauliny,\nDzień Matki', 'Augustyna, Magdaleny',
      'Jaromira, Justyny', 'Magdaleny, Urszuli', 'Karola, Jana', 'Anieli, Petroneli'],
    ['Wniebowstąpienie,\nDzień Dziecka', 'Marianny, Piotra', 'Leszka, Karola', 'Karola, Franciszka',
      'Bonifacego, Walerii', 'Norberta, Bogumiła', 'Roberta, Wiesława', 'Medarda, Seweryna',
      'Pelagii, Dominika', 'Bogumiła, Małgorzaty', 'Radomiła, Feliksa', 'Janiny, Leona',
      'Lucjana, Antoniego', 'Elwiry, Michała', 'Wita, Jolanty', 'Aliny, Anety', 'Laury, Alberta',
      'Marka, Elżbiety', 'Gerwazego, Protazego', 'Diny, Florentyny', 'Alicji, Alojzego', 'Pauliny, Jana',
      'Wandy, Zenona,\nDzień Ojca', 'Jana, Danuty', 'Lucji, Doroty', 'Jana, Pawa', 'Władysława, Cyryla',
      'Leona, Ireneusza', 'Piotra, Pawła', 'Emilii, Lucyny'], ['Haliny, Marcina', 'Jagody, Marii',
        'Jacka, Tomasza', 'Odona, Elżbiety', 'Marii, Antoniego',
        'Dominiki, Lucji', 'Cyryla, Metodego', 'Elżbiety, Eugeniusza', 'Weroniki, Zenona',
        'Witalisa, Antoniego',
        'Olgi, Benedykta', 'Jana, Bonifacego', 'Henryka, Andrzeja', 'Bonawentury, Kamila',
        'Henryka, Włodzimierza', 'Mariki, Benity', 'Anety, Jadwigi', 'Kamila, Szymona', 'Wincentego, Marcina',
        'Czesława, Małgorzaty', 'Daniela, Wawrzyńca', 'Marii, Magdaleny', 'Bogny, Brygidy', 'Kingi, Krystyny',
        'Krzysztofa, Jakuba', 'Anny, Grażyny', 'Julii, Natalii', 'Marceli, Wiktora',
        'Marty, Ludmiły', 'Piotra, Aldony', 'Ignacego, Heleny'], ['Justyny, Juliana', 'Kariny, Gustawa',
          'Lidii, Augusta', 'Protazego, Jana', 'Marii, Mariana', 'Sławy, Jakuba', 'Kajetana, Doroty',
          'Cypriana, Dominiki', 'Romana, Edyty', 'Borysa, Wawrzyńca', 'Zuzanny, Lecha', 'Lecha, Euzebii',
          'Diany, Poncjana', 'Alfreda, Maksymiliana', 'Święto Wojska Polskiego,\nWniebowzięcie NMP',
          'Stefana, Joachima', 'Mirona, Jacka', 'Ilony, Heleny', 'Bolesława, Juliana',
          'Bernarda, Sobiesława', 'Joanny, Kazimiery', 'Cezarego, Tymoteusza', 'Apolinarego, Filipa',
          'Bartosza, Haliny', 'Ludwika, Józefa', 'Marii, Aleksandra', 'Cezarego, Moniki', 'Patrycji, Augustyna',
          'Beaty, Jana', 'Róży, Feliksa', 'Bogdana, Rajmunda'], ['Idziego, Bronisława', 'Juliana, Stefana',
            'Grzegorza, Szymona', 'Julianny, Róży', 'Doroty, Wawrzyńca', 'Beaty, Eugeniusza', 'Domosławy, Reginy',
            'Marii, Adrianny', 'Sergiusza, Piotra',
            'Lukasza, Aldony', 'Jacka, Hiacynta', 'Radzimira, Marii', 'Eugenii, Jana', 'Bernarda, Cypriana',
            'Nikodema, Marii', 'Edyty, Cypriana', 'Roberta, Justyna', 'Stanisława, Ireny', 'Konstancji, Teodora',
            'Filipiny, Eustachego', 'Jonasza, Mateusza', 'Tomasza, Maurycego', 'Bogusława, Linusa',
            'Gerarda, Tomiry', 'Aurelii, Władysława', 'Wawrzyńca, Damiana', 'Wincentego, Justyny',
            'Wacława, Marka', 'Michała, Rafała', 'Honoriusza, Hieronima,\nDzień Chłopaka'],
    ['Danuty, Teresy', 'Teofila, Sławomira', 'Teresy, Jana', 'Rozalii, Franciszka', 'Placyda, Apolinarego',
      'Artura, Brunona', 'Marii, Marka', 'Brygidy, Walerii', 'Dionizego, Wincentego', 'Pauliny, Leona',
      'Aleksandra, Dobromiry', 'Eustachego, Maksymiliana', 'Edwarda, Honorata',
      'Liwii, Bernarda,\nDzień Nauczyciela', 'Jadwigi, Teresy', 'Gawła, Ambrożego', 'Wiktora, Ignacego',
      'Juliana, Łukasza', 'Jana, Pawła', 'Ireny, Jana', 'Urszuli, Jakuba', 'Halki, Filipa',
      'Marleny, Seweryna', 'Rafała, Marcina', 'Darii, Bonifacego', 'Lucjana, Damiana', 'Iwony, Sabiny',
      'Szymona, Tadeusza', 'Euzebii, Felicjana', 'Zenobii, Przemysława', 'Urbana, Krzysztofa'],
    ['Serweryna, Wiktoryny,\nWszystkich Świętych', 'Bohdana, Bożydara,\nZaduszki', 'Sylwii, Huberta',
      'Karola, Olgierda', 'Elżbiety, Dominika', 'Feliksa, Leonarda', 'Antoniego, Ernesta',
      'Bogdana, Klaudiusza', 'Aleksandra, Teodora', 'Leny, Leona', 'Marcina, Teodora,\nŚwięto Niepodległości',
      'Renaty, Witolda', 'Mateusza, Stanisława', 'Serafina, Wawrzyńca', 'Alberta, Leopolda',
      'Edmunda, Marii', 'Grzegorza, Elżbiety', 'Romana, Karoliny', 'Seweryny, Maksyma', 'Anatola, Rafała',
      'Alberta, Konrada', 'Cecylii, Stefana', 'Adelii, Klemensa', 'Flory, Emmy', 'Erazma, Katarzyny',
      'Sylwestra, Konrada', 'Waleriana, Maksyma', 'Lesława, Stefana', 'Błażeja, Saturnina',
      'Andrzeja, Konstantego'], ['Natalii, Edmunda', 'Balbiny, Pauliny', 'Franciszka, Ksawerego',
        'Barbary, Jana', 'Sabiny, Edyty', 'Mikołaja, Emiliana', 'Marcina, Teodora', 'Marii, Makarego',
        'Wiesława, Joanny', 'Julii, Bogdana', 'Waldemara, Daniela', 'Aleksandra, Ady', 'Lucji, Otylii',
        'Alfreda, Jana', 'Celiny, Waleriana', 'Zdzisławy, Alicji', 'Olimpii, Floriana', 'Gracjana, Bogusława',
        'Gabrieli, Dariusza', 'Bogumiły, Dominika', 'Seweryna, Piotra', 'Zenona, Franciszki', 'Wiktorii, Jana',
        'Adama, Ewy,\nWigilia', 'Boże Narodzenie', 'II Dzień\nBożego Narodzenia',
        'Jana, Żanety', 'Teofilii, Cezarego', 'Dawida, Tomasza', 'Eugeniusza, Irmy', 'Sylwestra, Mariusza']],
    days: ['Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota', 'Niedziela'],
    daysInMonth: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
    months: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień',
      'Październik', 'Listopad', 'Grudzień'],
    fonts: ['Akronim', 'Allura', 'AmaticSC', 'Amita', 'Anton', 'Arbutus', 'Arial', 'Audiowide', 'Autour One',
      'Berkshire Swash', 'Bigelow Rules', 'Black Ops One', 'Bowlby One SC', 'Bree Serif', 'Butcherman',
      'Caveat Brush', 'Caveat', 'Clicker Script', 'Coda', 'Combo', 'Corben', 'Courgette', 'Croissant One',
      'Cutive Mono', 'Dynalight', 'Eagle Lake', 'Eater', 'Emblema One', 'Exo', 'Freckle Face', 'Fruktur',
      'Grand Hotel', 'Great Vibes', 'Gruppo', 'Hanalei Fill', 'Hanalei', 'Inconsolata', 'Italianno', 'Itim',
      'Jim Nightshade', 'Joti One', 'Just Me Again Down Here', 'Kalam', 'Kaushan Script', 'Kelly Slab',
      'Limelight', 'Marck Script', 'Modak', 'New Rocker', 'Nosifer', 'Open Sans Condensed', 'Open Sans',
      'Parisienne', 'Patrick Hand SC', 'Patrick Hand', 'Petit Formal Script', 'Pirata One', 'Plaster',
      'Playfair Display', 'Poiret One', 'Quintessential', 'Ranchers', 'Ranga', 'Ribeye Marrow', 'Risque',
      'Roboto Mono', 'Romanesco', 'Ruslan Display', 'Sacramento', 'Sarina', 'Shadows Into Light Two',
      'Shojumaru', 'Sonsie One', 'Source Code Pro', 'Stalemate', 'Tahoma', 'Times New Roman', 'Tillana',
      'Titan One', 'Titillium Web', 'Ubuntu', 'Underdog', 'Unica One'],
    JSONExportProperties: ['height', 'width', 'background', 'objects', 'mask', 'originalHeight', 'originalWidth',
      'originalScaleX', 'originalScaleY', 'originalLeft', 'originalTop', 'lineHeight', 'lockMovementX',
      'lockMovementY', 'lockScalingX', 'lockScalingY', 'lockUniScaling', 'lockRotation', 'lockObject', 'id',
      'isTinted', 'filters', 'cx', 'cy', 'cw', 'ch', 'cs', 'orgSrc', 'originalW', 'originalH', 'skewX', 'skewY'],
    shapeDefaults: angular.extend({originX: 'center', originY: 'center', fill: '#0088cc'}, objectDefaults),
    textDefaults: angular.extend({
      originX: 'center',
      scaleX: 1,
      scaleY: 1,
      fontFamily: 'Times New Roman',
      fontSize: 40,
      fill: '#454545',
      textAlign: 'center'
    }, objectDefaults)
  };
}]);
'use strict';
angular.module('common.fabric.utilities', []).directive('parentClick', ['$timeout', function($timeout) {
  return {
    scope: {parentClick: '&'}, link: function(scope, element) {
      element.mousedown(function() {
        $timeout(function() {
          scope.parentClick();
        });
      }).children().mousedown(function(e) {
        e.stopPropagation();
      });
    }
  };
}]).factory('Keypress', [function() {
  var self = {};
  self.onSave = function(cb) {
    $(document).keydown(function(event) {
      if ((event.ctrlKey || event.metaKey) && event.which === 83) {
        event.preventDefault();
        cb();
        return false;
      }
    });
  };
  return self;
}]).filter('reverse', [function() {
  return function(items) {
    if (items)return items.slice().reverse();
  };
}]);
angular.module('common.fabric.dirtyStatus', []).service('FabricDirtyStatus', ['$window', function($window) {
  var self = {dirty: false};

    /**
     * check save status
     * @return {string}
     */
  function checkSaveStatus() {
    if (self.isDirty())return 'Twoje zmiany są niezapisane. Czy napewno chcesz wyjść ze strony?';
  }

  self.endListening = function() {
    $window.onbeforeunload = null;
    $window.onhashchange = null;
  };
  self.startListening = function() {
    $window.onbeforeunload = checkSaveStatus;
    $window.onhashchange = checkSaveStatus;
  };
  self.isDirty = function() {
    return self.dirty;
  };
  self.setDirty = function(value) {
    self.dirty = value;
  };
  return self;
}]);
angular.module('common.fabric.directive', ['common.fabric.canvas']).directive('fabric', ['$timeout', 'FabricCanvas',
  function($timeout, FabricCanvas) {
    return {
      scope: {fabric: '='}, controller: function($scope, $element) {
        FabricCanvas.setElement($element);
        FabricCanvas.createCanvas();
      }
    };
  }]).directive('fabricStatic', ['$timeout', 'FabricCanvas', function($timeout, FabricCanvas) {
    return {
      scope: {
        fabric: '=',
        id: '@id'
      }, controller: function($scope) {
        FabricCanvas.createStaticCanvas($scope.id);
      }
    };
  }]);
