'use strict';
angular.module('kApp').controller('PhotosController', ['$scope', '$http', '$modal', '$timeout',
  function($scope, $http, $modal, $timeout) {
    $scope.file_path = '';
    $scope.showUsed = 1;
    $scope.directory = 0;
    $scope.array = [];
    $scope.selected = null;
    $scope.catalog_id = 0;
    $scope.ownerHash = null;

    $scope.dbClickItem = function(item) {
      if (item.catalog) {
        $scope.array = [];
        $scope.directory = item.id;
        $scope.catalog_id = item.id;
        localStorage.setItem('catalog', item.id);
        $scope.getPhotos();
      } else {
        $scope.fabric.insertPhoto($scope.selected.filename);
      }
    };

    $scope.insertAsBackground = function() {
      $scope.$parent.drawBackground({
        type: 0,
        src: $scope.selected.filename.replace(/(\.[\w\d_-]+)$/i, '_b' + '$1')
      });
    };

    $scope.insertRandomly = function() {
      $scope.$parent.save();
      $scope.fabric.insertRandom($('.photo-item').length, $scope.array);
    };

    $scope.openPhotoUpload = function() {
      var uid = $scope.$parent ? $scope.$parent.uid : null;
      $modal.open({
        templateUrl: '/partials/photo_upload.html',
        controller: 'UploadController',
        resolve: {
          uid: function() {
            return uid;
          }
        }
      }).result.then(function(selectedItem) {
        $scope.selected = selectedItem;
      }, function() {
        $scope.getPhotos();
      });
    };
    $scope.$on('open:photoUpload', function() {
      $timeout($scope.openPhotoUpload, 1000);
    });
    $scope.getPhotos = function() {
      $http({
        method: 'POST',
        url: '/user/get/photos',
        data: {
          catalog: $scope.directory,
          userId: getUserId(),
          ownerHash: localStorage.getItem('ownerHash')
        }
      }).success(function(response) {
        if ($scope.showUsed) {
          $scope.array = response.photos;
          return;
        }

        var indexes = _.map($scope.fabric.getCanvasPhotos(), function(o) {
          return o.orgSrc.match(/\d+/g).pop();
        });

        $scope.array = _.filter(response.photos, function(o) {
          return $.inArray(o.src.match(/\d+/g).pop(), indexes) < 0;
        });
      });
    };
    $scope.hideOrShowUsed = function() {
      $scope.showUsed = !$scope.showUsed;
      $scope.getPhotos();
    };
    $scope.addFolder = function() {
      $http({
        method: 'POST',
        url: '/user/add/catalog',
        data: {
          parent_id: $scope.catalog_id,
          ownerHash: $scope.ownerHash
        }
      }).success(function() {
        $scope.getPhotos();
      });
    };
    $scope.removeItem = function(item) {
      $http({
        method: 'POST',
        url: '/user/remove/item',
        data: {
          id: item.id
        }
      }).success(function() {
        $scope.getPhotos();
      });
    };
    $scope.setActive = function(item) {
      for (var i = 0; i < $scope.array.length; i += 1) {
        $scope.array[i].selected = 0;
      }
      item.selected = 1;
      $('#preview').remove();
      $scope.selected = item;
      $scope.$parent.fabric.dragInit = true;
      $scope.$parent.fabric.dragHolder = item.src;
    };
    $scope.hidePhotos = function() {
      $http({
        method: 'POST',
        url: '/user/hide/photos',
        data: {
          photos: _.map($scope.fabric.getCanvasPhotos(), function(o) {
            return o.orgSrc.match(/\d+/g).pop();
          })
        }
      }).success(function() {
        $scope.getPhotos();
      });
    };
    $scope.showPreview = function(item, e) {
      if (/folder/.test(item.src)) {
        return;
      }

      $('#preview').remove();
      $('body').append('<p id=\'preview\'><img src=\'' + item.src.replace('_s', '_m') +
                '\' alt=\'Image preview\' /></p>');
      $('#preview').css({
        top: (e.pageY - 20) + 'px',
        left: (e.pageX + 20) + 'px'
      }).fadeIn('fast');
    };
    $scope.movePreview = function(e) {
      $('#preview').css({
        top: (e.pageY - 20) + 'px',
        left: (e.pageX + 20) + 'px'
      });
    };
    $scope.hidePreview = function() {
      $('#preview').remove();
    };
    $scope.goUp = function() {
      $http({
        method: 'POST',
        url: '/user/get/photosUp',
        data: {
          catalog: $scope.catalog_id,
          ownerHash: $scope.ownerHash
        }
      }).success(function(response) {
        $scope.array = response.photos;
        $scope.catalog_id = response.catalog;
      });
    };
    $scope.ok = function() {
      $modalInstance.close($scope.selected);
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
    $scope.init = function() {
      $scope.getPhotos();
      localStorage.setItem('catalog', 0);
      $scope.ownerHash = localStorage.getItem('ownerHash');
    };
    $scope.init();
  }]);
