'use strict';

angular.module('kApp').controller('TextController', ['$scope', function($scope) {
  $scope.execute = 0;
  $scope.lineHeight = 1;
  $scope.fontSize = 30;
  $scope.font = 'Times New Roman';
  $scope.$on('text:selected', function(event, args) {
    $scope.lineHeight = parseFloat(args.lineHeight);
    $scope.fontSize = args.fontSize;
    $scope.font = args.font;
    if (args.color.indexOf('rgb') > -1) {
      var rgb = args.color.replace('rgb(', '').replace(')', '').split(',');
      $.jPicker.List[0].color.active.val('rgb', {r: parseInt(rgb[0]), g: parseInt(rgb[1]), b: parseInt(rgb[2])});
    } else {
      $.jPicker.List[0].color.active.val('hex', args.color);
    }
  });
  $scope.init = function() {
    clearJPicker();

    $('#text-color-picker').jPicker({
      window: {
        title: ' ',
        effects: {
          type: 'show',
          speed: {
            show: 'slow',
            hide: 'fast'
          }
        },
        position: {
          x: 'screenCenter',
          y: '0'
        }
      },
      color: {
        alphaSupport: false
      }
    },
    function(color) {
      $scope.$parent.fabric.setFill(color.val('all').hex);
    });

    $('.icon-italic').off('click').on('click', function() {
      $scope.$parent.fabric.toggleItalic();
    });
    $('.icon-bold').off('click').on('click', function() {
      $scope.$parent.fabric.toggleBold();
    });
    $('.icon-underline').off('click').on('click', function() {
      $scope.$parent.fabric.toggleUnderline();
    });
    $('.icon-strikethrough').off('click').on('click', function() {
      $scope.$parent.fabric.toggleLinethrough();
    });
    $('.icon-paragraph-left').off('click').on('click', function() {
      $scope.$parent.fabric.setTextAlign('left');
    });
    $('.icon-paragraph-center').off('click').on('click', function() {
      $scope.$parent.fabric.setTextAlign('center');
    });
    $('.icon-paragraph-right').off('click').on('click', function() {
      $scope.$parent.fabric.setTextAlign('right');
    });
    $('.icon-paragraph-justify').off('click').on('click', function() {
      $scope.$parent.fabric.setTextAlign('justify');
    });
  };
  $scope.init();
}]);
