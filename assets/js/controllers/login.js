'use strict';
angular.module('kApp').controller('LoginController', ['$scope', '$modalInstance', '$http', '$modal',
  function($scope, $modalInstance, $http, $modal) {
    $scope.error = false;
    $scope.actionLogin = function(username, password) {
      $http({
        method: 'POST',
        url: '/login_check',
        data: {
          _username: username,
          _password: password,
          _remember_me: false
        }
      }).success(function(response) {
        if (response.id == null) {
          $scope.error = true;
        } else {
          $modalInstance.close({
            username: username,
            user_id: response.id
          });
        }
      });
    };
    $scope.ok = function() {
      $modalInstance.close();
    };
    $scope.openRegister = function() {
      $modalInstance.dismiss('cancel');
      $modal.open({
        templateUrl: '/partials/register.html',
        controller: 'RegisterController'
      });
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  }]);
