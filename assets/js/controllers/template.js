'use strict';
angular.module('kApp').controller('TemplateController', ['$scope', '$http', function($scope, $http) {
  $scope.displayed = 0; // 0 - templates, 1- layouts
  $scope.array = [{items: []}, {items: []}];
  $scope.getPages = function() {
    $http({
      method: 'POST',
      data: {format_id: $scope.$parent.format_id},
      url: '/user/get/template/pages'
    }).success(function(data) {
      $scope.array[0].items = data.templates;
    });
  };
  $scope.getLayouts = function() {
    $http({
      method: 'POST',
      data: {format_id: $scope.$parent.format_id},
      url: '/user/get/layouts'
    }).success(function(data) {
      $scope.array[1].items = data.layouts;
    });
  };
  $scope.loadTemplate = function(id) {
    $http({
      method: 'POST',
      url: '/user/load/template/page',
      data: {id: id}
    }).success(function(response) {
      if ($scope.displayed === 1) {
        $scope.$parent.fabric.loadLayout(response.pageContent.objects);
        return;
      }

      $scope.$parent.frames[JSON.parse(response.pageNumber)] = response.pageContent;
      $scope.$parent.drawPage();
    });
  };
  $scope.init = function() {
    $scope.getPages();
    $scope.getLayouts();
  };
  $scope.init();
}]);
