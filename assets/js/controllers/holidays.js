'use strict';
angular.module('kApp').controller('HolidaysController', ['$scope', '$modalInstance', 'userHolidays',
  function($scope, $modalInstance, userHolidays) {
    $scope.userHolidays = userHolidays;
    $scope.ok = function() {
      $modalInstance.close($scope.userHolidays);
    };
    $scope.add = function() {
      var form = $('.holidays-panel-body');
      $scope.userHolidays.push({
        name: form.find('[name="name"]').val(),
        date: form.find('[name="date"]').val(),
        color: form.find('.color-picker').val(),
        symbol: form.find('.selected-icon img').attr('src')
      });
      form.find('input').val('');
      $.jPicker.List[0].color.active.val('hex', '#000');
    };

    $scope.init = function () {
      setTimeout(function() {
        var iconSelect = new IconSelect("my-icon-select",
            {
              'selectedIconWidth': 18,
              'selectedIconHeight': 18,
              'selectedBoxPadding': 0,
              'iconsWidth': 28,
              'iconsHeight': 28,
              'boxIconSpace': 1,
              'verticalIconNumber': 4,
              'horizontalIconNumber': 4
            });

        var icons = [
          {'iconFilePath': '/icons/1.png', 'iconValue': '1'},
          {'iconFilePath': '/icons/2.png', 'iconValue': '2'},
          {'iconFilePath': '/icons/3.png', 'iconValue': '3'},
          {'iconFilePath': '/icons/4.png', 'iconValue': '4'},
          {'iconFilePath': '/icons/5.png', 'iconValue': '5'},
          {'iconFilePath': '/icons/6.png', 'iconValue': '6'},
          {'iconFilePath': '/icons/7.png', 'iconValue': '7'},
          {'iconFilePath': '/icons/8.png', 'iconValue': '8'},
          {'iconFilePath': '/icons/9.png', 'iconValue': '9'}
        ];

        iconSelect.refresh(icons);
      });
    };

    $scope.init();
  }]);
