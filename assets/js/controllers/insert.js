'use strict';
angular.module('kApp').controller('InsertController', ['$scope', '$http', function($scope, $http) {
  var shapeOptionsGroup = $('.shape-options-group');
  $scope.execute = 0;
  $scope.displayed = 0;
  $scope.categories = [];
  $scope.selected = [];
  $scope.indexOfCategory = [0, 0, 0];
  $scope.array = [{category: []}, {category: []}, {category: []}];
  $scope.fillType = 0;
  var colorFirst = '#000';

  $scope.getCliparts = function() {
    $http({
      method: 'GET',
      url: '/user/get/cliparts'
    }).success(function(data) {
      $scope.array[0].category = data.cliparts;
      var _cat = [];
      $scope.selected[0] = $scope.array[0].category[0].name;
      $scope.array[0].category.forEach(function(item) {
        _cat.push(item.name);
      });
      $scope.categories.push(_cat);
    });
  };
  $scope.getBackgrounds = function() {
    $http({
      method: 'POST',
      url: '/user/get/backgrounds',
      data: {
        format_id: $scope.$parent.format_id
      }
    }).success(function(data) {
      $scope.array[1].category = data.backgrounds;
      var _cat = [];
      $scope.array[1].category.forEach(function(item) {
        _cat.push(item.name);
      });
      $scope.categories.push(_cat);
      $scope.selected = [$scope.categories[0][0], $scope.categories[1][0]];
      $scope.displayed = 1;
    });
  };
  $scope.$on('object:selected', function() {
    var object = $scope.$parent.fabric.getObject();
    if (!object || object.type !== 'path-group') {
      return;
    }
    var w = object.paths[0].get('strokeWidth');
    $scope.strokeBool = (w !== 0);

    for (var i in object.paths) {
      object.paths[i].set('strokeOpacity', 1);
    }

    object.set('strokeWidth', 0);
    object.set('stroke', '#000000');
    $scope.borderWidth = parseInt(w);

    object.rgb2hex = function(rgb) {
      rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
      return rgb && rgb.length === 4 ? '#' + ('0' + parseInt(rgb[1], 10).toString(16)).slice(-2) +
            ('0' + parseInt(rgb[2], 10).toString(16)).slice(-2) +
            ('0' + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
    };

    if (!object.get('fill')) {
      var color = object.get('fill');
      switch (color.type) {
        case 'linear':
        case 'radial':
          $('#second-fill-picker').removeClass('hidden');
          $scope.fillType = color.type === 'linear' ? 1 : 2;
          break;
        default:
          $scope.fillType = 0;
          break;
      }
            // $scope.color.opacityHex = object.rgb2hex(color.colorStops[0].color);
            //  $scope.secondFillColor = object.rgb2hex(color.colorStops[1].color);
    } else {
            // $scope.color.opacityHex = object.get('fill');
      $('#second-fill-picker').addClass('hidden');
      $scope.fillType = 0;
    }
        // $scope.strokeColor = object.paths[0].get('stroke')
  });
  $scope.getShapes = function() {
    $http({
      method: 'GET',
      url: '/user/get/shapes'
    }).success(function(data) {
      $scope.array[2].category = data.shapes;
      $scope.displayed = 2;
    });
  };
  $scope.getBorderWidth = function(borderWidth, strokeBool) {
    if ($scope.execute === 0) {
      $scope.execute = true;
      return false;
    }

    if (strokeBool === true && borderWidth === 0) {
      borderWidth = 1;
    }

    $scope.borderWidth = strokeBool ? borderWidth : 0;
    $scope.$parent.fabric.setBorderWidth(strokeBool ? borderWidth : 0);
  };
  $scope.insert = function(item) {
    var src = item.src;

    switch(true) {
      case /cliparts/.test(src):
        $scope.$parent.fabric.addClipart(src);
        break;
      case /shapes/.test(src):
        $scope.$parent.fabric.createShape(src);
        break;
      case /backgrounds/.test(src):
        $scope.$parent.drawBackground(item);
        break;
    }
  };

  $scope.dragStart = function(item) {
    $scope.$parent.fabric.dragInit = true;
    $scope.$parent.fabric.dragHolder = item.src;
  };

  $scope.showPreview = function(item, e) {
    if ($scope.displayed !== 1) {
      return;
    }
    $('#preview').remove();
    $('body').append('<p id="preview" class="background-preview"><img src="' + item.src.replace('_s', '_m')
            + '" alt="Image preview" /></p>');
    $('#preview').css({
      top: (e.pageY - 20) + 'px',
      left: (e.pageX + 20) + 'px'
    }).fadeIn('fast');
  };
  $scope.movePreview = function(e) {
    if ($scope.displayed !== 1) {
      return;
    }
    $('#preview').css({
      top: (e.pageY - 20) + 'px',
      left: (e.pageX + 20) + 'px'
    });
  };
  $scope.hidePreview = function() {
    if ($scope.displayed !== 1) {
      return;
    }
    $('#preview').remove();
  };
  $scope.handleCategoryClick = function(categoryId) {
    switch (categoryId) {
    case 1:
      $scope.displayed = 0;
      break;
    case 2:
      $scope.getBackgrounds();
      break;
    case 3:
      $scope.getShapes();
      break;
    }
  };

  $scope.updateCategory = function() {
    $scope.indexOfCategory[$scope.displayed] = $scope.categories[$scope.displayed]
            .indexOf($scope.selected[$scope.displayed]);
  };
  $scope.init = function() {
    $scope.getCliparts();

    $('#insert-menu').on('click', '.header-menu-link', function(e) {
      if ($(e.target).data('menu') === 'shapes') {
        shapeOptionsGroup.show().css('display', 'inline-block');
      } else {
        shapeOptionsGroup.hide();
      }
    });

    clearJPicker();

    $('#stroke-color-picker').jPicker({
      effects: {
        type: 'show',
        speed: {
          show: 'slow',
          hide: 'fast'
        }
      },
      position: {
        x: 'screenCenter',
        y: '400'
      },
      color: {
        alphaSupport: false
      }
    }, function(color) {
      $scope.$parent.fabric.setStrokeColor('#' + color.val('all').hex);
    });

    $('#fill-color-picker').jPicker({
      effects: {
        type: 'show',
        speed: {
          show: 'slow',
          hide: 'fast'
        }
      },
      position: {
        x: 'screenCenter',
        y: '400'
      },
      color: {
        alphaSupport: false
      }
    }, function(color) {
      colorFirst = color.val('all').hex;
      $scope.$parent.fabric.fillShape('#' + colorFirst);
    });

    $('#second-fill-color-picker').jPicker({
      effects: {
        type: 'show',
        speed: {
          show: 'slow',
          hide: 'fast'
        }
      },
      position: {
        x: 'screenCenter',
        y: '400'
      },
      color: {
        alphaSupport: false
      }
    }, function(color) {
      $scope.$parent.fabric.fillLinearGradient('#' + colorFirst, '#' + color.val('all').hex);
    });
  };
  $scope.init();
}]);
