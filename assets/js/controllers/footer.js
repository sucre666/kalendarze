'use strict';

angular.module('kApp').controller('FooterController', ['$scope', 'Fullscreen',
  function($scope, Fullscreen) {
    $scope.slider = {
      options: {
        stop: function() {
        }
      },
      value: 55
    };
    $scope.previousFrame = function() {
      if ($scope.$parent.currentFrame <= 0
                || $scope.$parent.fabric.isLoading()) {
        return;
      }
      $scope.$parent.save();
      $scope.$parent.currentFrame -= 1;
      $scope.$parent.drawPage();
    };
    $scope.nextFrame = function() {
      if ($scope.$parent.currentFrame >= $scope.$parent.frames.length - 1
                || $scope.$parent.fabric.isLoading()) {
        return;
      }
      $scope.$parent.save();
      $scope.$parent.currentFrame += 1;
      $scope.$parent.drawPage();
    };
    $scope.setFullScreen = function() {
      if (Fullscreen.isEnabled()) {
        Fullscreen.cancel();
      } else {
        Fullscreen.all();
      }
    };
    $scope.dicreaseSize = function() {
      if ($scope.$parent.fabric.canvasScale <= .4) {
        return;
      }
      $scope.$parent.setZoom($scope.$parent.fabric.canvasScale - .2);
    };
    $scope.increaseSize = function() {
      if ($scope.$parent.fabric.canvasScale >= 1.5) {
        return;
      }
      $scope.$parent.setZoom($scope.$parent.fabric.canvasScale + .2);
    };
    $scope.isActiveFooterTab = function(page) {
      return parseInt(page, 10) === $scope.$parent.view;
    };
  }]);
