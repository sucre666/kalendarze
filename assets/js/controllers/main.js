'use strict';

var template,
  projectCopy = [],
  ownerHash = null,
  hashUser,
  userName;

var clearJPicker = function() {
  for (var i = $.jPicker.List.length - 1; i >= 0; i--) {
    var id = $.jPicker.List[i].id;
    $.jPicker.List[i].destroy();
    $('#' + id).parent().find('span.jPicker').remove();
  }
};

var easterForYear = function(year) {
  var a, b, c, d, e, f, g, h, i, k, l, m, n0, n, p;
  a = year % 19;
  b = Math.floor(year / 100);
  c = year % 100;
  d = Math.floor(b / 4);
  e = b % 4;
  f = Math.floor((b + 8) / 25);
  g = Math.floor((b - f + 1) / 3);
  h = (19 * a + b - d - g + 15) % 30;
  i = Math.floor(c / 4);
  k = c % 4;
  l = (32 + 2 * e + 2 * i - h - k) % 7;
  m = Math.floor((a + 11 * h + 22 * l) / 451);
  n0 = h + l + 7 * m + 114;
  n = Math.floor(n0 / 31) - 1;
  p = n0 % 31 + 1;
  return new Date(year, n, p);
};
var getUserId = function() {
  return hashUser;
};
var getUsername = function() {
  return userName;
};
var setUserId = function(id) {
  hashUser = id;
};
var setUsername = function(username) {
  userName = username;
};
var guid = function() {
  var s4 = function() {
    return Math.floor((1 + Math.random()) * 65536).toString(16).substring(1);
  };

  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4();
};
angular.module('kApp').controller('MainController', ['$scope', 'Fabric', 'FabricConstants', '$modal', '$http',
  '$location', '$timeout', '$rootScope', function($scope, Fabric, FabricConstants, $modal, $http, $location,
                                                     $timeout, $rootScope) {
    $scope.fabric = {};
    $scope.view = 2;
    $scope.frames = [];
    $scope.currentFrame = 0;
    $scope.previewName = '';
    $scope.loaded = false;
    $scope.uid = 0;
    $scope.icon = 'icon-layout';
    $scope.format_id = 0;
    $scope.template_id = 1;
    $scope.isCalendar = 0;
    $scope.pages = [];
    $scope.userHolidays = [];

    var scrollAmount = 0, scrollCurrent = 0, toggledMore = false;

    window.addEventListener('click', function(e) {
      if (toggledMore && !e.target.closest('#header-template-wrapper')) {
        var target = document.querySelector('.header-categories-frame-wrapper-active');
        if (target) {
          target.classList.remove('header-categories-frame-wrapper-active');
          target.scrollTop = scrollCurrent = 0;
        }

        toggledMore = false;
      }
    });
    $scope.templateArrowUp = function($event) {
      scrollCurrent -= 1;
      if (scrollCurrent <= 0) {
        scrollCurrent = 0;
      }
      $event.currentTarget.parentNode.previousElementSibling.scrollTop = scrollCurrent * 112;
    };
    $scope.templateArrowDown = function($event) {
      scrollCurrent += 1;
      scrollAmount = parseInt($event.currentTarget.parentNode.previousElementSibling.scrollHeight / 107) - 1;
      if (scrollCurrent >= scrollAmount) {
        scrollCurrent = scrollAmount;
      }
      $event.currentTarget.parentNode.previousElementSibling.scrollTop = scrollCurrent * 112;
    };
    $scope.templateArrowMore = function($event) {
      scrollAmount = parseInt($event.currentTarget.parentNode.previousElementSibling.scrollHeight / 107) - 1;
      toggledMore = true;
      if (scrollAmount <= 0) {
        return;
      }
      $event.currentTarget.parentNode.previousElementSibling.classList.add('header-categories-frame-wrapper-active');
    };

    $scope.clearBackground = function() {
      $scope.fabric.clearBackground();
      $scope.frames[$scope.currentFrame].background = '{source: "", repeat: "repeat"}';
      $scope.frames[$scope.currentFrame].backgroundImg = '';
    };
    $scope.drawBackground = function(item) {
      $scope.clearBackground();
      if (item.type) {
        $scope.drawBackgroundPattern(item.src);
      } else {
        $scope.drawBackgroundImage(item.src);
      }
    };
    $scope.drawBackgroundPattern = function(path) {
      $scope.seter = function(src, callback) {
        var i = $scope.fabric.canvasBackgroundColor;
        $scope.fabric.setCanvasBackgroundPattern({source: src, repeat: 'repeat'});
        var interval = setInterval(function() {
          if (i !== $scope.fabric.canvasBackgroundColor) {
            clearInterval(interval);
            callback();
          }
        }, 100);
        return true;
      };
      $scope.seter(path, function() {
        $scope.fabric.render();
        $scope.frames[$scope.currentFrame].background = $scope.fabric.canvasBackgroundColor;
      });
    };
    $scope.drawBackgroundImage = function(path) {
      $scope.seter = function(src, callback) {
        var i = $scope.fabric.canvasBackgroundImage;
        $scope.fabric.setCanvasBackgroundImage(src);
        var interval = setInterval(function() {
          if (i !== $scope.fabric.canvasBackgroundImage && $scope.fabric.canvasBackgroundImage !== null) {
            clearInterval(interval);
            callback();
          }
        }, 100);
        return true;
      };
      $scope.seter(path, function() {
        $scope.fabric.render();
        $scope.frames[$scope.currentFrame].backgroundImg = $scope.fabric.canvasBackgroundImage;
      });
    };
    $scope.navigateTo = function(page) {
      $scope.save();
      $scope.view = parseInt(page, 10);
    };
    $scope.openCustomCalendar = function() {
      $modal.open({
        templateUrl: '/partials/custom_calendar.html',
        controller: 'CustomController',
        resolve: {
          styles: function() {
            return $scope.frames[$scope.currentFrame].settings;
          }
        }
      }).result.then(function(settings) {
        for (var i = 0; i < $scope.frames.length; i += 1) {
          template[i].settings = settings;
        }
        $scope.computeFrames();
      });
    };
    $scope.openHelp = function() {
      $modal.open({
        templateUrl: '/partials/help.html'
      });
    };
    $scope.openBasket = function() {
      if (!getUserId()) {
        return $scope.openLogin();
      }

      $modal.open({
        templateUrl: '/partials/basket.html',
        controller: 'OrderController',
        resolve: {
          userDetails: function() {
            return $scope.username;
          }
        }
      });
    };
    $scope.openAddHolidays = function() {
      $modal.open({
        templateUrl: '/partials/add_holidays.html',
        controller: 'HolidaysController',
        backdrop: false,
        resolve: {
          userHolidays: function() {
            return $scope.userHolidays;
          }
        }
      }).result.then(function(holidays) {
        $scope.userHolidays = holidays;
      });
    };
    $scope.openLogin = function() {
      $modal.open({
        templateUrl: '/partials/login.html',
        controller: 'LoginController'
      }).result.then(function(response) {
        $scope.username = response.username;
        $scope.user_id = response.user_id;
        setUsername(response.username);
        setUserId(response.user_id);
      });
    };
    $scope.openTemplateDialog = function() {
      $modal.open({
        templateUrl: '/partials/templateDialog.html',
        controller: 'FormatController',
        backdrop: false
      }).result.then(function(template) {
        document.getElementById('template-dialog').remove();
        var id = template.id;
        if (template.type) {
          $scope.loadProject(id);
        } else {
          $scope.loadTemplate(id, template.formatId);
          $scope.template_id = id;
        }
      });
    };

    $scope.save = function() {
      $scope.fabric.clearGrid();
      var x = JSON.parse($scope.fabric.getJSON());
      $scope.frames[$scope.currentFrame].objects = x.objects;
    };
    var holidays = {
        year: '',
        days: [],
        labels: []
      },
      currentDate = new Date();
    $scope.clear = function() {
      $scope.frames[$scope.currentFrame] = angular.copy(projectCopy[$scope.currentFrame]);
      $scope.drawPage();
    };
    $scope.clearAll = function() {
      for (var i = 0; i < $scope.frames.length; i += 1) {
        $scope.frames[i] = angular.copy(projectCopy[i]);
      }
      $scope.drawPage();
    };
    $scope.saveProject = function() {
      if (!getUserId()) {
        $scope.openLogin();
        return false;
      }
      $scope.save();
      var projectName = projectName.length == 0 ? prompt('Podaj nazwę projektu') : projectName;

      if (!projectName) {
        return false;
      }

      $scope.notice = {
        'text': 'Twój projekt jest zapisywany...',
        'class': 'info'
      };
      $http({
        method: 'POST',
        url: '/user/save/template',
        data: {
          uid: $scope.uid,
          name: projectName,
                    // user_id: user_id(),
          content: JSON.stringify($scope.frames),
          icon: $scope.icon,
          holidays: JSON.stringify($scope.userHolidays),
          template_id: $scope.template_id
        }
      }).success(function() {
        $scope.notice = {
          'text': 'Twój projekt został zapisany. Możesz teraz przejść do koszyka, aby sfinalizować zakup ' +
                    'lub wykonać kolejny projekt.',
          'class': 'success'
        };
        $timeout(function() {
          $scope.notice = '';
        }, 5E3);
      });
    };
    $scope.makePDF = function() {
      $scope.setZoom(3);
      $timeout(function() {
        var imgData = $scope.fabric.drawPDF(),
          w = $scope.fabric.canvasWidth * .010416667 / 3,
          h = $scope.fabric.canvasHeight * .010416667 / 3,
          format = 'a4',
          marginLeft = (8.3 - w) / 3,
          marginTop = 0.3;
                // var o = w < h ? "p": "l";
        if ($scope.isCalendar) {
          format = [
            w + (($scope.document.padding.left + $scope.document.padding.right) * .010416667),
            h + (($scope.document.padding.top + $scope.document.padding.bottom) * .010416667)
          ];
          marginLeft = $scope.document.padding.left * .010416667;
          marginTop = $scope.document.padding.top * .010416667;
        }


        var pdf = new jsPDF({
          orientation: 'portrait',
          unit: 'in',
          format: format
        });

        pdf.addImage(imgData, 'JPEG', marginLeft, marginTop, w, h);

        pdf.save('projekt.pdf');
        $scope.setZoom(1);
      }, 1E3);
    };

    $scope.$on('canvas:clearAll', function() {
      $scope.clearAll();
    });

    $scope.historySave = function() {
      $scope.save();
      var entry = _.map($scope.frames, function(frame) {
        return _.omit(frame, ['settings']);
      });
      if (sessionStorage.getItem('history') === null) {
        sessionStorage.setItem('history', JSON.stringify([entry]));
      } else {
        var rows = JSON.parse(sessionStorage.getItem('history'));
        if (rows.length > 5) {
          rows.splice(0, 1);
        }
        if ($scope.fabric.currentChange > 0) {
          rows.splice(rows.length - $scope.fabric.currentChange, rows.length -
                        (rows.length - $scope.fabric.currentChange));
          $scope.fabric.currentChange = 0;
        }

        rows.push(entry);
        sessionStorage.setItem('history', JSON.stringify(rows));
      }
    };

        //
        // Listeners
        // ================================================================
    $scope.$on('history:load', function() {
      $timeout(function() {
        if (sessionStorage.getItem('history') === null) {
          return;
        }
        var entries = JSON.parse(sessionStorage.getItem('history'));
        var length = entries.length - 1;
        if ($scope.fabric.currentChange > length || $scope.fabric.currentChange >= 10) {
          $scope.fabric.currentChange = length;
          return;
        }
        if ($scope.fabric.currentChange < 0) {
          $scope.fabric.currentChange = 0;
          return;
        }
        $scope.frames = _.extend($scope.frames, entries[length - $scope.fabric.currentChange]);
        $scope.drawPage();
      });
    });

    $scope.$on('image:insert', function() {
      $modal.open({
        templateUrl: '/partials/insert_photo.html',
        controller: 'PhotoInsertController'
      }).result.then(function(selectedItem) {
        $scope.fabric.insertPhoto(selectedItem);
      });
    });

    $scope.$on('image:crop', function(e, args) {
      $modal.open({
        templateUrl: '/partials/crop.html',
        controller: 'CropController',
        resolve: {
          image: function() {
            return args;
          }
        }
      }).result.then(function(cropData) {
        $scope.fabric.fitPhoto(cropData);
      });
    });

        //
        // Canvas Size
        // ================================================================
    $scope.setZoom = function(value) {
      $scope.fabric.canvasScale = value;
      $scope.fabric.setZoom();

      var item = $scope.frames[$scope.currentFrame].background;
      if (item && item.source) {
        $scope.drawBackgroundPattern(item.source);
      }

      item = $scope.frames[$scope.currentFrame].backgroundImg;
      if (item && item._element) {
        $scope.drawBackgroundImage(item._element.src);
      }
    };
    $scope.months = {};
    $scope.selectedMonth = {start: 0, end: 0};
    $scope.initMonthSelects = function() {
      $scope.months = {
        start: [],
        end: []
      };

      for (var i = 0; i < 13; i += 1) {
        $scope.months.start.push($scope.fabric.months[i % 12] + ' ' +
                    (currentDate.getFullYear() + parseInt(i / 12, 10)));
      }
      for (var i = $scope.selectedMonth.start + 11; i < 24; i += 1) {
        $scope.months.end.push($scope.fabric.months[i % 12] + ' ' +
                    (currentDate.getFullYear() + parseInt(i / 12, 10)));
      }
      $scope.selectedMonth.end = $scope.months.end.length - 1;
      $scope.computeFrames();
    };
    $scope.computeFrames = function() {
      var page, n, framesCount = 25 - $scope.selectedMonth.start -
                ($scope.months.end.length - $scope.selectedMonth.end - 1);
      $scope.prepareFrames(framesCount);
      projectCopy = [];
      projectCopy.push(angular.copy($scope.frames[0]));
      for (page = 1; page < framesCount; page += 1) {
        for (n = $scope.frames[page].objects.length - 1; n >= 0; n -= 1) {
          if ($scope.frames[page].objects[n].mt === 1) {
            $scope.frames[page].objects.splice(n, 1);
          }
        }
        $scope.generateMonth(($scope.selectedMonth.start + page - 1) % 12,
                    ($scope.selectedMonth.start + page - 1) / 12 === 0 ?
                        currentDate.getFullYear() : currentDate.getFullYear() +
                    parseInt(($scope.selectedMonth.start + page - 1) / 12, 10), page);
        projectCopy.push(angular.copy($scope.frames[page]));
      }
    };
    $scope.prepareFrames = function(count) {
      var i = 0;
      if ($scope.frames.length > count) {
        $scope.frames = $scope.frames.slice(0, count);
      } else {
        for (i = $scope.frames.length; i < count; i += 1) {
          $scope.frames.push(angular.copy(template[i]));
        }
      }
      $scope.currentFrame = 0;
      $scope.drawPage();
    };
    $scope.calculateHolidays = function(year) {
      var day = easterForYear(year);
      holidays.year = year;
      holidays.days = angular.copy($scope.fabric.holidays);
      holidays.labels = angular.copy($scope.fabric.labels);
      day.setDate(day.getDate() - 1);
      holidays.days[day.getMonth()].push(day.getDate());
      holidays.labels[day.getMonth()][day.getDate()] += '\nWielkanoc';
      day.setDate(day.getDate() - 52);
      holidays.labels[day.getMonth()][day.getDate()] += '\nTłusty Czwartek';
      day.setDate(day.getDate() + 5);
      holidays.labels[day.getMonth()][day.getDate()] += '\nOstatki';
      day.setDate(day.getDate() + 1);
      holidays.labels[day.getMonth()][day.getDate()] += '\nŚroda Popielcowa';
      day.setDate(day.getDate() + 39);
      holidays.labels[day.getMonth()][day.getDate()] += '\nNiedziela Palmowa';
      day.setDate(day.getDate() + 4);
      holidays.labels[day.getMonth()][day.getDate()] += '\nWielki Czwartek';
      day.setDate(day.getDate() + 1);
      holidays.labels[day.getMonth()][day.getDate()] += '\nWielki Piątek';
      day.setDate(day.getDate() + 1);
      holidays.labels[day.getMonth()][day.getDate()] += '\nWielka Sobota';
      day.setDate(day.getDate() + 2);
      holidays.days[day.getMonth()].push(day.getDate());
      holidays.labels[day.getMonth()][day.getDate()] += '\nPoniedziałek Wielkanocny';
      day.setDate(day.getDate() + 48);
      holidays.labels[day.getMonth()][day.getDate()] += '\nZesłanie Ducha Świętego';
      day.setDate(day.getDate() + 12);
      holidays.days[day.getMonth()].push(day.getDate());
      day.setDate(day.getDate() - 1);
      holidays.labels[day.getMonth()][day.getDate()] += '\nBoże Ciało';
    };
    $scope.generateMonth = function(month, year, page) {
      var i, firstDay, total, day, color, date = new Date(year, month, 1);
      $scope.frames[page].name = $scope.fabric.months[month];
      $scope.frames[page].month = month;
      $scope.frames[page].year = year;
      var objectIndex = 0;
      if (year !== holidays.year) {
        $scope.calculateHolidays(year);
      }
      if (date.getYear() % 4 === 0) {
        FabricConstants.daysInMonth[1] = 29;
      }
      $scope.frames[page].objects.push({
        type: 'text',
        id: page * 1E3 + objectIndex,
        text: FabricConstants.months[month],
        fontSize: template[page].settings[3].fontSize,
        fontWeight: template[page].settings[3].fontWeight,
        fontFamily: template[page].settings[3].fontFamily,
        left: template[page].settings[3].marginLeft,
        top: template[page].settings[3].marginTop,
        shadow: template[page].settings[3].shadow,
        fill: template[page].settings[3].fill,
        originX: template[page].settings[3].originX,
        selectable: false,
        textAlign: template[page].settings[3].textAlign,
        mt: 1
      });
      objectIndex += 1;
      $scope.frames[page].objects.push({
        type: 'text',
        id: page * 1E3 + objectIndex,
        text: year.toString(),
        fontSize: template[page].settings[4].fontSize,
        fontWeight: template[page].settings[4].fontWeight,
        fontFamily: template[page].settings[4].fontFamily,
        left: template[page].settings[4].marginLeft,
        top: template[page].settings[4].marginTop,
        shadow: template[page].settings[4].shadow,
        originX: template[page].settings[4].originX,
        fill: template[page].settings[4].fill,
        selectable: false,
        textAlign: template[page].settings[4].textAlign,
        mt: 1
      });
      objectIndex += 1;
      for (i = 0; i < 7; i += 1) {
        color = i === 6 ? '#F00' : template[page].settings[2].fill;
        $scope.frames[page].objects.push({
          type: 'text',
          id: page * 1E3 + objectIndex,
          text: FabricConstants.days[i],
          fontSize: template[page].settings[2].fontSize,
          fontWeight: template[page].settings[2].fontWeight,
          fontFamily: template[page].settings[2].fontFamily,
          left: template[page].settings[2].marginLeft + template[page].settings[2].spaceColumn * i,
          top: template[page].settings[2].marginTop,
          fill: color,
          selectable: false,
          originX: template[page].settings[2].originX,
          textAlign: template[page].settings[2].textAlign,
          mt: 1
        });
        objectIndex += 1;
      }
      firstDay = (date.getDay() + 6) % 7;
      total = FabricConstants.daysInMonth[month] + firstDay;
      for (i = 0; i < total; i += 1) {
        day = i + 1 - firstDay;
        if (i + 1 <= firstDay) {
          continue;
        }
        color = i % 7 === 6 ? '#F00' : holidays.days[month].indexOf(day) >= 0 ?
                    '#F00' : template[page].settings[0].fill;
        $scope.frames[page].objects.push({
          type: 'text',
          id: page * 1E3 + objectIndex,
          text: holidays.labels[month][day - 1],
          fontSize: template[page].settings[0].fontSize,
          fontWeight: template[page].settings[0].fontWeight,
          fontFamily: template[page].settings[0].fontFamily,
          left: template[page].settings[0].marginLeft + template[page].settings[0].spaceColumn * (i % 7),
          top: template[page].settings[0].marginTop +
                    template[page].settings[0].spaceRow * parseInt(i / 7, 10),
          fill: color,
          selectable: false,
          originX: template[page].settings[0].originX,
          textAlign: template[page].settings[0].textAlign,
          mt: 1
        });
        objectIndex += 1;
        $scope.frames[page].objects.push({
          type: 'text',
          id: page * 1E3 + objectIndex,
          text: day.toString(),
          fontSize: template[page].settings[1].fontSize,
          fontWeight: template[page].settings[1].fontWeight,
          fontFamily: template[page].settings[1].fontFamily,
          left: template[page].settings[1].marginLeft + template[page].settings[1].spaceColumn * (i % 7),
          top: template[page].settings[1].marginTop +
                    template[page].settings[1].spaceRow * parseInt(i / 7, 10),
          fill: color,
          selectable: template[page].settings[1].selectable,
          originX: template[page].settings[1].originX,
          textAlign: template[page].settings[1].textAlign,
          mt: 1
        });
        objectIndex += 1;
      }
    };
    $scope.drawPage = function() {
      $scope.fabric.clearCanvas();
      $scope.fabric.loadJSON($scope.frames[$scope.currentFrame]);
    };
    $scope.loadProject = function(id) {
      if (!id) {
        return;
      }

      $http({
        method: 'POST',
        url: '/user/load/project',
        data: {
          id: id
        }
      }).success(function(response) {
        $scope.load(response);
        $scope.userHolidays = JSON.parse(response.holidays);
      });
    };
    $scope.loadTemplate = function(id, formatId) {
      $http({
        method: 'POST',
        url: '/user/load/template',
        data: {
          id: id,
          formatId: formatId
        }
      }).success(function(response) {
        $scope.load(response);
      });
    };
    $scope.loadTemplateByHash = function(hash) {
      $http({
        method: 'POST',
        url: '/user/load',
        data: {
          hash: hash
        }
      }).success(function(response) {
        if (response.code == 200) {
          $scope.load(response);
        } else {
          $scope.openTemplateDialog();
        }
      }).error(function() {
        $scope.openTemplateDialog();
      });
    };
    $scope.load = function(response) {
      $scope.frames = JSON.parse(response.template.content);
      template = JSON.parse(response.template.content);
      $scope.document = {
        size: {
          width: response.format.width,
          height: response.format.height
        },
        padding: {
          top: response.padding.top,
          right: response.padding.right,
          bottom: response.padding.bottom,
          left: response.padding.left
        }
      };
      $scope.format_id = response.format.id;
      $scope.fabric.setCanvasSize($scope.document.size.width, $scope.document.size.height);
      $scope.isCalendar = response.format.isCalendar;
      if ($scope.isCalendar) {
        $scope.initMonthSelects();
      } else {
        $scope.currentFrame = 0;
        $scope.drawPage();
      }

      $scope.loaded = true;
      $rootScope.$broadcast('open:photoUpload');
    };
    $scope.changePageName = function(value) {
      $scope.$apply(function() {
        $scope.previewName = value;
      });
    };

    $scope.init = function() {
      $scope.uid = guid();
      $scope.user_id = getUserId();
      $scope.username = getUsername();
      $scope.fabric = new Fabric({
        fonts: FabricConstants.fonts,
        fontSizes: FabricConstants.fontSizes,
        interline: FabricConstants.interline,
        months: FabricConstants.months,
        daysInMonth: FabricConstants.daysInMonth,
        days: FabricConstants.days,
        labels: FabricConstants.labels,
        holidays: FabricConstants.holidays
      });
      if (getUserId() == 0) {
        if (localStorage.getItem('ownerHash') == null) {
          ownerHash = guid();
          localStorage.setItem('ownerHash', ownerHash);
        } else {
          ownerHash = localStorage.getItem('ownerHash');
        }
      }

      WebFont.load({
        google: {
          families: ['Akronim', 'Allura', 'AmaticSC', 'Amita', 'Anton', 'Arbutus',
            'Audiowide', 'Autour One', 'Berkshire Swash', 'Bigelow Rules', 'Black Ops One',
            'Bowlby One SC', 'Bree Serif', 'Butcherman', 'Caveat Brush', 'Caveat',
            'Clicker Script', 'Coda', 'Combo', 'Corben', 'Courgette', 'Croissant One',
            'Cutive Mono', 'Dynalight', 'Eagle Lake', 'Eater', 'Emblema One', 'Exo',
            'Freckle Face', 'Fruktur', 'Grand Hotel', 'Great Vibes', 'Gruppo', 'Hanalei Fill',
            'Hanalei', 'Inconsolata', 'Italianno', 'Itim', 'Jim Nightshade', 'Joti One',
            'Just Me Again Down Here', 'Kalam', 'Kaushan Script', 'Kelly Slab', 'Limelight',
            'Marck Script', 'Modak', 'New Rocker', 'Nosifer', 'Open Sans Condensed', 'Open Sans',
            'Parisienne', 'Patrick Hand SC', 'Patrick Hand', 'Petit Formal Script', 'Pirata One',
            'Plaster', 'Playfair Display', 'Poiret One', 'Quintessential', 'Ranchers', 'Ranga',
            'Ribeye Marrow', 'Risque', 'Roboto Mono', 'Romanesco', 'Ruslan Display', 'Sacramento',
            'Sarina', 'Shadows Into Light Two', 'Shojumaru', 'Sonsie One', 'Source Code Pro',
            'Stalemate', 'Tillana', 'Titan One', 'Titillium Web', 'Ubuntu', 'Underdog', 'Unica One']
        }
      });

      switch ($location.search().action) {
      case 'load':
        var id = $location.search().id;
        if (id) {
          $scope.loadProject(parseInt(id));
        }
        break;
      default:
        var code = $location.search().hash;
        $location.path('/photos');
        if (code) {
          $scope.loadTemplateByHash(code);
        } else if ($scope.frames.length == 0) {
          $scope.openTemplateDialog();
        } else {
          $scope.fabric.setCanvasSize($scope.document.size.width, $scope.document.size.height);
          $scope.currentFrame = 0;
          $scope.loaded = true;
          $scope.drawPage();
        }
      }
    };
    $scope.$on('canvas:created', $scope.init);
  }]);

