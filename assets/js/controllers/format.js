'use strict';
angular.module('kApp').controller('FormatController', ['$scope', '$http',
  '$modalInstance', function($scope, $http, $modalInstance) {
    $scope.categories = {};
    $scope.projects = {};
    $scope.slider = {
      options: {
        stop: function() {
        }
      }, value: 75
    };
    $scope.selected = {
      category: 0,
      item: 0,
      template: 0
    };
    $scope.preview = false;
    $scope.template = {};

    $scope.getTemplates = function() {
      $http({
        method: 'GET',
        url: '/user/get/all/templates'
      }).success(function(data) {
        $scope.categories = data.templates;
        $scope.template = $scope.categories[$scope.selected.category]
                .items[$scope.selected.item].templates[$scope.selected.template];
      });
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
    $scope.ok = function(id) {
      var selectedCategory = $scope.categories[$scope.selected.category];
      if (!selectedCategory) {
        return;
      }

      $modalInstance.close({
        id: id,
        type: 0,
        formatId: selectedCategory.items[$scope.selected.item].format_id
      });
    };
    var initialised = false, slider;
    $scope.$on('ngRepeatFinished', function() {
      if (!initialised) {
        slider = $('#bx-slider2').bxSlider({
          slideWidth: 500,
          adaptiveHeight: true,
          infiniteLoop: false,
          hideControlOnEnd: true,
          mode: 'fade',
          pager: false,
          onSliderLoad: function() {
            $scope.$apply(function() {
              $scope.preview = true;
            });
          }
        });
        initialised = true;
      } else {
        slider.reloadSlider();
      }
    });
    $scope.getProjects = function() {
      $http({
        method: 'GET',
        url: '/user/get/projects'
      }).success(function(data) {
        $scope.projects = data.projects;
      });
    };
    $scope.removeProject = function(project, index) {
      $http({
        method: 'POST',
        url: '/user/remove/project',
        data: {id: project.id}
      }).success(function() {
        $scope.projects.splice(index, 1);
      });
    };
    $scope.loadProject = function(id) {
      $modalInstance.close({
        id: id,
        type: 1
      });
    };
    $scope.changeCategory = function(category) {
      $scope.selected.category = category;
      $scope.selected.item = 0;
      $scope.selected.template = 0;

      var items = $scope.categories[$scope.selected.category].items;

      if (items.length === 0) {
        return;
      }

      if (items[$scope.selected.item].templates.length) {
        $scope.template = items[$scope.selected.item].templates[$scope.selected.template];
      }

      $scope.preview = false;
    };
    $scope.changeDocument = function(document) {
      $scope.selected.item = document;
      $scope.selected.template = 0;

      var templates = $scope.categories[$scope.selected.category].items[$scope.selected.item].templates;

      if (templates.length) {
        $scope.preview = true;
        $scope.template = templates[$scope.selected.template];
      } else {
        $scope.preview = false;
      }
    };
    $scope.changeTemplate = function(template) {
      $scope.selected.template = template;
      $scope.template = $scope.categories[$scope.selected.category]
            .items[$scope.selected.item].templates[$scope.selected.template];
    };
    $scope.init = function() {
      $scope.getTemplates();
      $scope.getProjects();
    };
    $scope.init();
  }]);
