'use strict';
angular.module('kApp').controller('PreviewController', ['$scope', 'FabricConstants', 'FabricCanvas',
  function($scope, FabricConstants, FabricCanvas) {
    $scope.preview = {};
    $scope.canvases = [];
    $scope.canvasScale = 1;
    $scope.width = 0;
    $scope.height = 0;
    $scope.init = function() {
      $scope.width = $scope.$parent.document.size.width * $scope.canvasScale - 1;
      $scope.height = $scope.$parent.document.size.height * $scope.canvasScale - 1;
      $scope.$parent.loaded = false;
      var i = 0;
      while (i < $scope.$parent.frames.length) {
        $scope.canvases.push({
          id: 'preview-' + i
        });
        i += 1;
      }
    };
    $scope.init();
    $scope.addObjects = function(page) {
      var item = $scope.frames[page].background;
      if (item && item.source != '') {
        FabricCanvas.canvas.setBackgroundColor($scope.frames[page].background.source);
      }
      var item2 = $scope.frames[page].backgroundImg;
      if (item2) {
        fabric.Image.fromURL(item2.orgSrc || item2.src, function(img) {
          img.set({
            width: $scope.width,
            height: $scope.height,
            originX: 'left',
            originY: 'top'
          });
          FabricCanvas.canvas.setBackgroundImage(img, FabricCanvas.canvas.renderAll.bind(FabricCanvas.canvas));
        });
      }

      if ($scope.$parent.frames[page].objects.length === 0) {
        return;
      }

      fabric.util.enlivenObjects($scope.$parent.frames[page].objects, function(objects) {
        objects.forEach(function(o) {
          o.left = o.left * $scope.canvasScale;
          o.top = o.top * $scope.canvasScale;
          o.width = o.width * o.scaleX;
          o.height = o.height * o.scaleY;
          o.scaleX = $scope.canvasScale;
          o.scaleY = $scope.canvasScale;
          if (o.type == 'image') {
            if (o.src.match(/img\/upload/) == null) {
              o.applyFilters(function() {
                FabricCanvas.canvas.renderAll();
              });
              FabricCanvas.canvas.add(o);
            }
          } else if (o.mt != 4) {
            FabricCanvas.canvas.add(o);
          }
        });
        FabricCanvas.canvas.renderAll();
      });
    };
    $scope.$on('ngRepeatFinished', function() {
      $scope.$parent.loaded = true;
      $scope.drawSlide(0);
      var slider = $('#bx-slider').bxSlider({
        slideWidth: $scope.width + 30,
        adaptiveHeight: false,
        infiniteLoop: false,
        hideControlOnEnd: true,
        pagerType: 'short',
        mode: 'fade',
        onSlideBefore: function() {
          $scope.drawSlide(slider.getCurrentSlide());
        }
      });
    });
    $scope.drawSlide = function(page) {
      FabricCanvas.createStaticCanvas('preview-' + page);
      $scope.$parent.changePageName($scope.$parent.frames[page].name);
      FabricCanvas.canvas.setWidth($scope.width);
      FabricCanvas.canvas.setHeight($scope.height);
      $scope.addObjects(page);
      FabricCanvas.canvas.renderAll();
    };
  }]);
