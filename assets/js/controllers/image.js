'use strict';
angular.module('kApp').controller('ImageController', ['$scope', '$http', function($scope, $http) {
  $scope.array = [{items: []}, {items: []}, {items: []}];
  $scope.displayed = 1;
  $scope.angle = 0;
  $scope.slider = {
    brightness: {
      options: {
        stop: function() {
          $scope.$parent.fabric.setFilter('brightness', $scope.slider.brightness.value);
        }
      },
      value: 0,
      disable: true
    }, contrast: {
      options: {
        stop: function() {
          $scope.$parent.fabric.setFilter('contrast', $scope.slider.contrast.value);
        }
      },
      value: 0,
      disable: true
    }, alpha: {
      options: {
        stop: function() {
          $scope.$parent.fabric.setOpacity($scope.slider.alpha.value);
        }
      },
      value: 1,
      disable: true
    }
  };
  $scope.$on('object:selected', function(event, args) {
    $scope.slider.alpha.value = args.opacity;
    $scope.slider.contrast.value = args.contrast;
    $scope.slider.brightness.value = args.brightness;
    $scope.slider.contrast.disable = $scope.slider.brightness.disable = args.disable;
    $scope.slider.alpha.disable = false;
  });
  $scope.$on('object:deselected', function() {
    $scope.slider.contrast.disable = $scope.slider.brightness.disable = $scope.slider.alpha.disable = true;
  });
  $scope.getFrames = function() {
    $http({
      method: 'GET',
      url: '/user/get/frames'
    }).success(function(data) {
      $scope.array[0].items = data.frames;
    });
  };
  $scope.getMasks = function() {
    $http({
      method: 'GET',
      url: '/user/get/masks'
    }).success(function(data) {
      $scope.array[1].items = data.masks;
    });
  };
  $scope.getFilters = function() {
    $http({
      method: 'GET',
      url: '/user/get/filters'
    }).success(function(data) {
      $scope.array[2].items = data.filters;
    });
  };
  $scope.$on('object:rotating', function(event, args) {
    $scope.angle = parseInt(args.angle);
  });
  $scope.init = function() {
    $scope.getFrames();
    $scope.getMasks();
    $scope.getFilters();
    var object = $scope.$parent.fabric.getObject();
    if (!object || object.type !== 'image' || !object.filters) {
      return;
    }
    $scope.slider.alpha.value = object.opacity;
    $scope.slider.alpha.disable = false;

    if (object.filters[0]) {
      $scope.slider.brightness.value = parseInt(object.filters[0].brightness);
      $scope.slider.contrast.value = parseInt(object.filters[0].contrast);
      $scope.slider.contrast.disable = false;
      $scope.slider.brightness.disable = false;
    }
  };
  $scope.init();
}]);
