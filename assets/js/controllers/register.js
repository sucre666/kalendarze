'use strict';
angular.module('kApp').controller('RegisterController', ['$scope', '$modalInstance', '$http',
  function($scope, $modalInstance, $http) {
    $scope.error = false;
    $scope.ok = function() {
      $modalInstance.close();
    };
    $scope.actionRegister = function(username, email, password, passwordSecond, token) {
      $http({
        method: 'POST',
        url: '/register',
        headers: {'X-Requested-with': 'XMLHttpRequest'},
        data: {
          username: username,
          email: email,
          password_first: password,
          password_second: passwordSecond,
          _token: token
        }
      }).success(function(response) {
        $('.register-form-error').remove();
        if (response.code == 500) {
          for (var e in response.errors) {
            if ({}.hasOwnProperty.call(response.errors, e)) {
              $('#RegisterForm [data-type="' + e + '"]').prepend('<span class="register-form-error">' +
                            response.errors[e] + '</span>');
            }
          }
        } else {
          $('#RegisterForm').before('<span class="register-form-success">' +
                    'Konto zostało pomy\u0139\u009blnie założone.</span>')
                    .remove();
        }
      });
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  }]);
