'use strict';
var formats = [{name: 'Kalendarz', settingsIcons: [0, 2]}, {name: 'Wizytówka', settingsIcons: [1]}];
angular.module('kApp').controller('SettingsController', ['$scope', function($scope) {
  $scope.format = {type: 0};
  $scope.showIcons = function(containerIndex) {
    var show = false;
    formats[$scope.format.type].settingsIcons.forEach(function(index) {
      if (index === containerIndex) {
        show = true;
      }
    });
    if ($scope.$parent.user_id == 17 && containerIndex == 3) {
      return true;
    }
    return show;
  };
}]);
