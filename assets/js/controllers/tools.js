'use strict';
angular.module('kApp').controller('ToolsController', ['$scope', function($scope) {
  $scope.slider = {
    grid: {
      options: {
        stop: function() {
          $scope.setGrid();
        }
      },
      value: 50
    }
  };
  $scope.setGrid = function() {
    var gridValue = $scope.gridBool ? 100 - $scope.slider.grid.value : 0;

    $scope.$parent.fabric.grid = gridValue;
    $scope.$parent.fabric.gridOriginal = gridValue;
    $scope.$parent.fabric.clearGrid();

    if ($scope.gridBool) {
      $scope.$parent.fabric.drawGrid();
    } else {
      $scope.$parent.fabric.render();
    }
  };
  $scope.setDrag = function(dragToGrid) {
    $scope.$parent.fabric.dragToGrid = dragToGrid;
  };
  $scope.init = function() {
    if ($scope.$parent.fabric.grid > 0) {
      $scope.gridBool = true;
    }
    $scope.dragToGrid = $scope.$parent.fabric.dragToGrid;
  };
  $scope.init();
}]);
