'use strict';

angular.module('kApp').controller('OrderController', ['$scope', '$http', '$modal', '$modalInstance', 'userDetails',
  function($scope, $http, $modal, $modalInstance, userDetails) {
    $scope.projects = {};
    $scope.promo_codes = {};
    $scope.orders = [];
    $scope.orders_indexes = [];
    $scope.orders_count = 0;
    $scope.total_price = 0;
    $scope.templates = [];
    $scope.deliveries = [];
    $scope.delivery_price = 0;
    $scope.order_delivery = 0;
    $scope.total_sum = 0;
    $scope.client = {};
    $scope.addons = {};
    $scope.active_project_addons = 0;
    $scope.orders_history = {};
    $scope.currentDate = 0;
    $scope.promo = null;
    $scope.promo_value = null;
    $scope.parentController = angular.element($('body')).scope();
    $scope.new_code = false;
    $scope.ok = function() {
      $modalInstance.close(null);
    };
    $scope.makeOrder = function() {
      $http({
        method: 'POST',
        url: '/user/make/order',
        data: {
          client: $scope.client,
          products: $scope.orders_indexes,
          delivery: $scope.order_delivery,
          products_complete: $scope.orders,
          total_sum: $scope.total_sum,
          promo: $scope.promo
        }
      }).success(function(data) {
        $('input[data-type]').removeClass('order-error');
        $('.order-error-products').remove();
        if (data.errors != false) {
          for (var e in data.errors) {
            if ({}.hasOwnProperty.call(data.errors, e)) {
              $('input[data-type="' + e + '"]').addClass('order-error');
            }
          }
          if (data.errors['projects']) {
            $('.order-second-div').after('<span class="order-error-products">'
                        + data.errors['projects'] + '</span>');
          }
        } else {
          $('.order-second-div').after('<span class="order-success">Zamówienie zostało złożone.</span>');
        }

        if (!data.order_result) {
          return;
        }

        $scope.projects = {};
        $scope.promo_codes = {};
        $scope.orders = [];
        $scope.orders_indexes = [];
        $scope.orders_count = 0;
        $scope.total_price = 0;
        $scope.templates = [];
        $scope.deliveries = [];
        $scope.delivery_price = 0;
        $scope.order_delivery = 0;
        $scope.total_sum = 0;
        $scope.client = {};
        $scope.addons = {};
        $scope.active_project_addons = 0;
        $scope.send_scope();
        $scope.init();
      });
    };
    $scope.openAddons = function(id) {
      $scope.active_project_addons = id;
      $('#HiddenListAddons').show();
    };
    $scope.closeListProjects = function() {
      $('#HiddenList').hide();
    };
    $scope.closeListAddons = function() {
      $('#HiddenListAddons').hide();
    };
    $scope.addToAddons = function(id) {
      for (var e in $scope.orders) {
        if ($scope.orders[e].id != $scope.active_project_addons) {
          continue;
        }

        var aId = $scope.addons[id].id;
        var c = 0;
        if ($scope.orders[e].addons.length > 0) {
          for (var a in $scope.orders[e].addons) {
            if ($scope.orders[e].addons[a].id != aId) {
              return;
            }
            c++;
          }
        }
        if (c == 0) {
          $scope.orders[e].addons.push($scope.addons[id]);
        }
      }
      $scope.calcProductsPrice();
      $('#HiddenListAddons').hide();
    };
    $scope.removeAddon = function(projectId, addonId) {
      for (var o in $scope.orders) {
        if ($scope.orders[o].id != projectId) {
          return;
        }

        for (var a in $scope.orders[o].addons) {
          if ($scope.orders[o].addons[a].id != addonId) {
            return;
          }
          $scope.orders[o].addons.splice(a, 1);
        }
      }
      $scope.calcProductsPrice();
    };
    $scope.addToCart = function(id) {
      $('#HiddenList').hide();
      $scope.orders_indexes[id] = id;
      for (var p in $scope.projects) {
        if ({}.hasOwnProperty.call($scope.projects, p) && $scope.projects[p].id == id) {
          $scope.orders.push($scope.projects[p]);
          $scope.orders_count++;
        }
      }
      $('.order-error-products').remove();
      $('.order-success').remove();
      $scope.calcProductsPrice();
    };
    $scope.removeFromCart = function(id) {
      $scope.orders_indexes[id] = id;
      for (var p in $scope.orders) {
        if ({}.hasOwnProperty.call($scope.orders, p) && $scope.orders[p].id == id) {
          $scope.orders.splice(p, 1);
          $scope.orders_indexes.splice(id, 1);
        }
      }
      $scope.orders_count = 0;
      for (var o in $scope.orders) {
        if ({}.hasOwnProperty.call($scope.orders, o)) {
          $scope.orders_count += $scope.orders[o].quantity;
        }
      }
      $scope.calcProductsPrice();
    };
    $scope.changeDeliveryPrice = function(item) {
      $scope.delivery_price = item.price.toFixed(2);
      $scope.order_delivery = item.id;
      for (var i = 0; i < $scope.deliveries.length; i++) {
        $scope.deliveries[i].checked = $scope.deliveries[i].id == item.id;
      }
      $scope.calcProductsPrice();
    };
    $scope.spinnerChange = function() {
      var t = 0;
      for (var p in $scope.orders) {
        if ({}.hasOwnProperty.call($scope.orders, p)) {
          if ($scope.orders[p].quantity < 1) {
            $scope.orders[p].quantity = 1;
          }
          t += $scope.orders[p].quantity;
        }
      }
      $scope.orders_count = t;
      $scope.calcProductsPrice();
    };
    $scope.openProjectsList = function() {
      $('#HiddenList').show();
    };
    $scope.calcProductsPrice = function() {
      var t = 0;
      for (var p in $scope.orders) {
        var v = 0;
        for (var a in $scope.orders[p].addons) {
          v += $scope.orders[p].addons[a].price * $scope.orders[p].quantity;
        }
        t += $scope.templates[$scope.orders[p].template_id].price * $scope.orders[p].quantity;
        $scope.orders[p].addons_value = v;
        t += v;
      }
      $scope.total_price = t;
      $scope.total_sum = (parseFloat($scope.total_price) + parseFloat($scope.delivery_price)).toFixed(2);
      if ($scope.promo != null) {
        if ($scope.promo.type == '1') {
          $scope.promo_value = (0 - (t + parseFloat($scope.delivery_price)) *
                $scope.promo.value / 100).toFixed(2);
          t = t - t * $scope.promo.value / 100;
        } else {
          $scope.promo_value = (0 - $scope.promo.value).toFixed(2);
          t = t + parseFloat($scope.delivery_price) - $scope.promo.value;
        }
        $scope.total_sum = t < 0 ? 0 : t;
      }
      $scope.send_scope();
    };
    $scope.getProjects = function() {
      $http({
        method: 'GET',
        url: '/user/get/projects'
      }).success(function(data) {
        var p = data.projects;
        for (var pp in p) {
          p[pp].quantity = 1;
          p[pp].addons_value = 0;
          p[pp].addons = [];
        }
        $scope.projects = p;
        $scope.templates = data.templates;
        var d = data.deliveries;
        for (var dd in d) {
          if (dd == 0) {
            $scope.delivery_price = d[dd].price;
            $scope.order_delivery = d[dd].id;
            d[dd].checked = true;
          } else {
            d[dd].checked = false;
          }
        }
        $scope.deliveries = d;
      });
    };
    $scope.getPromoCodes = function() {
      $http({
        method: 'GET',
        url: '/user/get/promo_codes'
      }).success(function(data) {
        $scope.promo_codes = data.promo_codes;
        $scope.new_code = data.new_code;
      });
    };
    $scope.getAddons = function() {
      $http({
        method: 'GET',
        url: '/user/get/addons'
      }).success(function(data) {
        $scope.addons = data.addons;
      });
    };
    $scope.getOrders = function() {
      $http({
        method: 'GET',
        url: '/user/get/orders'
      }).success(function(data) {
        for (var o in data.orders) {
          data.orders[o].total_sum = data.orders[o].total_sum.toFixed(2);
        }
        $scope.orders_history = data.orders;
      });
    };
    $scope.savedProjectCopy = function(id) {
      for (var p in $scope.projects) {
        if ($scope.projects[p].id != id) {
          continue;
        }
        var item = $scope.projects[p];
        $http({
          method: 'POST',
          url: '/copy_project',
          headers: {'X-Requested-with': 'XMLHttpRequest'},
          data: {id: item.id}
        }).success(function(response) {
          if (response.projects) {
            $scope.projects.push(response.projects[0]);
          }
        });
      }
    };
    $scope.savedProjectDelete = function(id) {
      $http({
        method: 'POST',
        url: '/delete_project',
        headers: {'X-Requested-with': 'XMLHttpRequest'},
        data: {id: id}
      }).success(function(response) {
        if (!response.deleted) {
          return;
        }
        for (var p in $scope.projects) {
          if ($scope.projects[p].id == id) {
            $scope.projects.splice(p, 1);
          }
        }
      });
    };
    $scope.savedProjectEdit = function(id) {
      $scope.parentController.loadProject(id);
      $modalInstance.close(null);
    };
    $scope.savedProjectPreview = function(id) {
      $scope.parentController.loadProject(id);
      $modalInstance.close(null);
    };
    $scope.saveProjectTitle = function(id, name) {
      $http({
        method: 'POST',
        url: '/update_project_title',
        headers: {'X-Requested-with': 'XMLHttpRequest'},
        data: {id: id, name: name}
      }).success(function(response) {
        for (var p in $scope.projects) {
          if ($scope.projects[p].id == id) {
            $scope.projects[p].name = name;
          }
        }
      });
    };
    $scope.send_scope = function() {
      localStorage.setItem('orders', JSON.stringify($scope.orders));
      localStorage.setItem('orders_indexes', $scope.orders_indexes);
      localStorage.setItem('orders_count', $scope.orders_count);
      localStorage.setItem('total_price', $scope.total_price);
      localStorage.setItem('delivery_price', $scope.delivery_price);
      localStorage.setItem('order_delivery', $scope.order_delivery);
      localStorage.setItem('total_sum', $scope.total_sum);
      localStorage.setItem('active_project_addons', $scope.active_project_addons);
    };
    $scope.checkCode = function(id, value) {
      $http({
        method: 'POST',
        url: '/user/check/code',
        data: {
          id: id,
          code: value
        }
      }).success(function(response) {
        if (response.value) {
          $scope.promo = response;
          $scope.calcProductsPrice();
          $('input[data-type="promo"]').addClass('promo-good').prop('disabled', true);
        } else {
          $('input[data-type="promo"]').removeClass('promo-good');
        }
      });
    };
    $scope.init = function() {
      $scope.getProjects();
      $scope.getPromoCodes();
      $scope.getAddons();
      $scope.getOrders();
      $scope.username = userDetails;
      var date = new Date;
      var month = date.getMonth() + 1;
      if (month < 10) {
        month = '0' + month;
      }
      var day = date.getDate();
      if (day < 10) {
        day = '0' + day;
      }
      $scope.currentDate = date.getFullYear() + '-' + month + '-' + day;
      if (localStorage.getItem('orders')) {
        $scope.orders = JSON.parse(localStorage.getItem('orders'));
      }
      if (localStorage.getItem('orders_indexes')) {
        var t = localStorage.getItem('orders_indexes');
        t = t.split(',');
        $scope.orders_indexes = [];
        for (var i in t) {
          $scope.orders_indexes[i] = t[i];
        }
      }
      if (localStorage.getItem('orders_count')) {
            // $scope.orders_count = localStorage.getItem("orders_count");
        $scope.orders_count = 0;
        for (var o in $scope.orders) {
          $scope.orders_count += $scope.orders[o].quantity;
        }
      }
      $scope.total_price = localStorage.getItem('total_price') || $scope.total_price;
      $scope.delivery_price = localStorage.getItem('delivery_price') || $scope.delivery_price;
      $scope.order_delivery = localStorage.getItem('order_delivery') || $scope.order_delivery;
      $scope.total_sum = localStorage.getItem('total_sum') || $scope.total_sum;
      $scope.active_project_addons = localStorage.getItem('active_project_addons') || $scope.active_project_addons;
    };
    $scope.changeAccountPassword = function(passwordFirst, passwordSecond) {
      $http({
        method: 'POST',
        url: '/change_account_password',
        headers: {'X-Requested-with': 'XMLHttpRequest'},
        data: {
          password_first: passwordFirst,
          password_second: passwordSecond
        }
      }).success(function(response) {
        $scope.error = response.code == 500;
        $scope.info = response.code != 500;
      });
    };
    $scope.init();
  }]);
