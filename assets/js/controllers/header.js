'use strict';
angular.module('kApp').controller('HeaderController', ['$scope', '$location', function($scope, $location) {
  $scope.activeTab = function(route) {
    return $location.path() === route;
  };
}]);
