'use strict';
angular.module('kApp').controller('UploadController', ['$scope', '$modalInstance', 'FileUploader', 'uid',
  function($scope, $modalInstance, FileUploader, uid) {
    $scope.uid = uid;
    $scope.isUploadedAll = false;

    var uploader = $scope.uploader = new FileUploader({
      removeAfterUpload: true,
      url: '/upload?ownerHash=' + localStorage.getItem('ownerHash') + '&catalog=' + localStorage.getItem('catalog')
    });

    uploader.filters.push({
      name: 'imageFilter',
      fn: function(item, options) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
      }
    });

    uploader.onAfterAddingFile = function(fileItem) {
      fileItem.formData.push({name: fileItem.file.name});
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
      $scope.notice = {
        'text': 'Wystąpił błąd podczas wgrywania zdjęcia. Prosimy spróbować ponownie.',
        'class': 'error'
      };
    };

    uploader.onCompleteItem = function(fileItem, response, status, headers) {
      var text = '';
      var type = 'error';
      switch (status) {
      case 200:
        text = 'Twoje zdjęcia zostały wgrane na serwer i są teraz dostępne w zakładce "Moje zdjęcia".';
        type = 'success';
        break;
      case 0:
        text = 'Wgrywanie zdjęcia zostało anulowane';
        type = 'info';
        break;
      }

      if (text === '') {
        return;
      }

      $scope.notice = {
        'text': text,
        'class': type
      };
    };

    uploader.onCompleteAll = function() {
      $scope.isUploadedAll = true;
    };

    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
  }]);
