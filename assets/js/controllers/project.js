'use strict';
angular.module('kApp').controller('ProjectController', ['$scope', '$http', '$modalInstance',
  function($scope, $http, $modalInstance) {
    $scope.name = '';
    $scope.isLayout = false;

    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
    $scope.ok = function() {
      $modalInstance.close({
        name: $scope.name,
        is_layout: $scope.isLayout
      });
    };

    $scope.init = function() {
      if (userName == 'dominio') {
        $('is_layout').show();
      }
    };

    $scope.init();
  }]);
