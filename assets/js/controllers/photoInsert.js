'use strict';

angular.module('kApp').controller('PhotoInsertController', ['$scope', '$http', '$modal', '$modalInstance',
  function($scope, $http, $modal, $modalInstance) {
    $scope.file_path = '';
    $scope.showUsed = 1;
    $scope.directory = 0;
    $scope.array = [];
    $scope.selected = null;
    $scope.catalog_id = 0;
    $scope.dbClickItem = function(item) {
      if (item.catalog) {
        $scope.array = [];
        $scope.directory = item.id;
        $scope.catalog_id = item.id;
        localStorage.setItem('catalog', item.id);
        $scope.getPhotos();
      } else {
        $scope.ok();
      }
    };
    $scope.goUp = function() {
      $http({
        method: 'POST',
        url: '/user/get/photosUp',
        data: {catalog: $scope.catalog_id}
      }).success(function(response) {
        $scope.array = response.photos;
        $scope.catalog_id = response.catalog;
      });
    };
    $scope.getPhotos = function() {
      $http({
        method: 'POST',
        url: '/user/get/photos',
        data: {
          catalog: $scope.directory,
          userId: getUserId(),
          ownerHash: localStorage.getItem('ownerHash')
        }
      }).success(function(response) {
        $scope.array = response.photos;
      });
    };
    $scope.removeItem = function(item) {
      $http({
        method: 'POST',
        url: '/user/remove/item',
        data: {
          id: item.id
        }
      }).success(function() {
        $scope.getPhotos();
      });
    };
    $scope.setActive = function(item) {
      for (var i = 0; i < $scope.array.length; i += 1) {
        $scope.array[i].selected = 0;
      }
      item.selected = 1;
      $scope.selected = item;
    };
    $scope.ok = function() {
      if (!$scope.selected) {
        return;
      }

      if ($scope.selected.filename === '/img/folder.png') {
        $modalInstance.dismiss('cancel');
      } else {
        $modalInstance.close($scope.selected.filename);
      }
    };
    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    };
    $scope.init = function() {
      $scope.getPhotos();
      localStorage.setItem('catalog', 0);
      $scope.ownerHash = localStorage.getItem('ownerHash');
    };
    $scope.init();
  }]);
