'use strict';

angular.module('kApp').controller('CropController', ['$scope', '$http',
  '$modal', '$modalInstance', 'image', function($scope, $http, $modal, $modalInstance, image) {
    window.cropper = null;

    $scope.close = function() {
      $modalInstance.dismiss();
    };

    $scope.save = function() {
      $modalInstance.close(window.cropper.getCropBoxData());
    };

    $scope.init = function() {
      var img = document.createElement('img'),
        cropContainer = document.getElementById('crop-container');

      $(cropContainer).closest('.modal-dialog').css({
        width: image.width / image.cs + 'px'
      });

      img.id = 'image-source';
      img.src = image.src;
      img.style.visibility = 'hidden';
      img.style.maxWidth = '100%';

      cropContainer.appendChild(img);
      img.onload = function() {
        window.cropper = new Cropper(img, {
          movable: false,
          ready: function() {
            img.style.visibility = 'visible';

            window.cropper.setCropBoxData({
              left: image.cx / image.cs,
              top: image.cy / image.cs,
              width: image.cw / image.cs,
              height: image.ch /image.cs
            });

            document.querySelector('.cropper-crop-box').addEventListener('dblclick', function(e) {
              $modalInstance.close(window.cropper.getCropBoxData());
            });
          }
        });
      };
    };
  }]);
