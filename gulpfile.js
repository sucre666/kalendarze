var gulp = require('gulp'),
    concat = require('gulp-concat'),
    minify = require('gulp-minify'),
    minifycss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    eslint = require('gulp-eslint'),
    babel = require('gulp-babel'),
    autoprefixer = require('autoprefixer');

gulp.task('js:main', function() {
    gulp.src(['assets/js/controllers/*.js', 'assets/js/directives/*.js', 'assets/js/app.js'])
        .pipe(minify({
            mangle: false,
            noSource: true
        }))
        .pipe(concat('bundle.min.js'))
        .pipe(gulp.dest('web/js'));
});

gulp.task('js:vendor', function() {
    return gulp.src([
            'node_modules/bootstrap/dist/js/bootstrap.js',
            // 'node_modules/fabric/dist/fabric.js',
            'assets/js/vendor.js',
            'assets/js/vendor/*.js',
            'node_modules/jpicker/jpicker-1.1.6.js'])
        .pipe(minify({
            noSource: true
        }))
        .pipe(concat('vendor.min.js'))
        .pipe(gulp.dest('web/js'));
});
gulp.task('sass', function() {
    return gulp.src(['assets/sass/**/*'])
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([
            autoprefixer({
                browsers: ['> 1%', 'last 2 versions']
            })
        ]))
        .pipe(minifycss())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('web/css'));
});

gulp.task('js:lint', function() {
    return gulp.src(['assets/js/controllers/**/*', 'assets/js/app.js'])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('default', function() {
    gulp.start('sass', 'scripts');
});

gulp.task('scripts', function() {
    gulp.start('js:main', 'js:vendor');
});
