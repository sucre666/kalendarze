<?php

namespace AppBundle\Utils;

class ImageManager
{
    public static function resizeCropImage($source, $quality = 90)
    {
        try {
            $size = getimagesize($source);
            $pathInfo = pathInfo($source);

            $path = $pathInfo['dirname'] . '/' . $pathInfo['filename'];
            $ratio = $size[0] / $size[1];

            $fileQuality = intval(101 - (($size[0] * $size[1]) * 3) / filesize($source));
            $quality = $fileQuality < 90 ? $fileQuality : $quality;

            $dimensions = [
                [
                    'width' => 1 > $ratio ? 100 * $ratio : 100,
                    'height' => 1 <= $ratio ? 100 / $ratio : 100,
                    'path' => $path . '_s.' . $pathInfo['extension'],
                ],
                [
                    'width' => $size[0] / 2,
                    'height' => $size[1] / 2,
                    'path' => $path . '_m.' . $pathInfo['extension'],
                ],
                [
                    'width' => $size[0],
                    'height' => $size[1],
                    'path' => $path . '_b.' . $pathInfo['extension'],
                ],
            ];

            switch ($size['mime']) {
                case 'image/gif':
                    $sourceImage = imagecreatefromgif($source);
                    $image = "imagegif";
                    break;
                case 'image/png':
                    $sourceImage = imagecreatefrompng($source);
                    $image = 'imagepng';
                    $quality = intval($quality / 10);
                    break;
                case 'image/jpeg':
                    $sourceImage = imagecreatefromjpeg($source);
                    $image = "imagejpeg";
                    break;
                default:
                    return false;
                    break;
            }

            foreach ($dimensions as $dimension) {
                $destinationImage = imagecreatetruecolor($dimension['width'], $dimension['height']);

                if ($size['mime'] === 'image/png') {
                    imagealphablending($destinationImage, false);
                    imagesavealpha($destinationImage, true);
                }

                imagecopyresampled($destinationImage, $sourceImage, 0, 0, 0, 0, $dimension['width'], $dimension['height'],
                    $size[0], $size[1]);
                $image($destinationImage, $dimension['path'], $quality);

                if ($destinationImage) {
                    imagedestroy($destinationImage);
                }
            }

            if ($sourceImage) {
                imagedestroy($sourceImage);
            }
        } catch (\Exception $e) {
            var_dump($e->getMessage());die;
        }
    }

    public static function changeFilename($nr, $file)
    {
        return strtolower($nr . '.' . pathInfo($file, PATHINFO_EXTENSION));
    }
}
